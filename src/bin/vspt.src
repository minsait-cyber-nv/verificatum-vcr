#!/bin/sh

# Copyright 2008-2016 Douglas Wikstrom
#
# This file is part of Verificatum Core Routines (VCR). VCR is NOT free
# software. It is distributed under Verificatum License 1.0 and
# Verificatum License Appendix 1.0 for VCR.
#
# You should have agreed to this license and appendix when
# downloading VCR and received a copy of the license and appendix
# along with VCR. If not, then the license and appendix are available
# at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
# http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
#
# If you do not agree to the combination of this license and
# appendix, then you may not use VCR in any way and you must delete
# VCR immediately.

# There is a bug in the JDK that makes
# SecureRandom.getInstance("SHA1PRNG", "SUN") always seed itself from
# /dev/random. Thus, if it blocks, then getInstance blocks as well. It
# does not suffice to use -Djava.security.egd=file:/dev/urandom to
# avoid this problem. THE WEIRD-LOOKING DOT IN THE PATH IS REALLY
# NEEDED.

dnl The M4_JVM_DATAMODEL is replaced by -d32 or -d64, by an m4
dnl macro. The value used depends on the native wordlength. This is
dnl *necessary* in some OS, e.g., Solaris, since the JVM otherwise
dnl tries to execute in 32-bit mode instead of 64-bit mode. It is not
dnl needed in Linux or FreeBSD.
dnl
# Add the particular jar files we use.
export CLASSPATH=M4_ALL_JAR

# Add the location of the native libraries to the library variables.
export LIBRARY_PATH=M4_LIBDIR
export LD_LIBRARY_PATH=M4_LIBDIR

# Extract the name of this shell script without its path.
COMMAND_NAME=$0
COMMAND_NAME=${COMMAND_NAME##*/}

if [ x${VERIFICATUM_RANDOM_SOURCE} = x ]; then
   VERIFICATUM_RANDOM_SOURCE=~/.verificatum_random_source
fi

if [ x${VERIFICATUM_RANDOM_SEED} = x ]; then
   VERIFICATUM_RANDOM_SEED=~/.verificatum_random_seed
fi

java \
M4_JVM_DATAMODEL \
-Djava.security.egd=file:/dev/./urandom \
com.verificatum.arithm.SafePrimeTable \
"$COMMAND_NAME" "$VERIFICATUM_RANDOM_SOURCE" "$VERIFICATUM_RANDOM_SEED" "$@"
