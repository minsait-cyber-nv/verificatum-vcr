
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.eio;

/**
 * A reader of {@link ByteTree} instances.
 *
 * @author Douglas Wikstrom
 */
public final class ByteTreeReaderBT extends ByteTreeReader {

    /**
     * Underlying byte tree.
     */
    ByteTree bt;

    /**
     * Creates a reader of the given byte tree with the given parent.
     *
     * @param bt Underlying byte tree.
     */
    public ByteTreeReaderBT(final ByteTree bt) {
        this(null, bt);
    }

    /**
     * Creates a reader of the given byte tree with the given parent.
     *
     * @param parent Instance that spawned this one.
     * @param bt Underlying byte tree.
     */
    public ByteTreeReaderBT(final ByteTreeReader parent, final ByteTree bt) {
        super(parent, getRemaining(bt));
        this.bt = bt;
    }

    /**
     * Returns the number of children/bytes in the given byte tree.
     *
     * @param bt Underlying byte tree.
     * @return Number of children/bytes
     */
    private static int getRemaining(final ByteTree bt) {
        if (bt.value == null) {
            return bt.children.length;
        } else {
            return bt.value.length;
        }
    }

    // Documented in ByteTreeReader.java

    @Override
    public boolean isLeaf() {
        return bt.value != null;
    }

    @Override
    protected ByteTreeReader getNextChildInner() {
        final ByteTree child = bt.children[bt.children.length - remaining];
        return new ByteTreeReaderBT(this, child);
    }

    @Override
    protected void readInner(final byte[] destination,
                             final int offset,
                             final int length) {
        System.arraycopy(bt.value, bt.value.length - remaining, destination,
                         offset, length);
    }

    @Override
    public void close() {

        // Potentially this helps the garbage collect.
        this.bt = null;
    }
}
