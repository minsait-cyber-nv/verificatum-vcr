
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.eio;

/**
 * Interface capturing the ability of the instance of a class to be
 * marshalled into a byte tree with type information that can later be
 * restored to the instance. This is useful to, e.g., be able to
 * obliviously recover an instance of a subclass of {@link
 * com.verificatum.arithm.PGroup} of the precise subclass.
 *
 * <p>
 *
 * <b>Convention.</b> For this to be possible we require by convention
 * that every class that implements this interface also implements a
 * static method with one of the following signatures:
 *
 * <p>
 *
 * <code>
 * public static {@link Object}
 *     newInstance({@link ByteTreeReader} btr,
 *                 {@link com.verificatum.crypto.RandomSource} rs,
 *                 int certainty)
 * </code>
 * <br>
 * <code>
 * public static {@link Object} newInstance({@link ByteTreeReader} btr)
 * </code>
 *
 * <p>
 *
 * The first method allows probabilistically checking the input using
 * the given source of randomness. The error probability should be
 * bounded by <i>2<sup>- <code>certainty</code></sup></i>.
 *
 * @author Douglas Wikstrom
 */
public interface Marshalizable extends ByteTreeConvertible {

    /**
     * Returns a brief human friendly description of this instance.
     * This is merely a comment which can not be used to recover the
     * instance.
     *
     * @param verbose Decides if the description should be verbose or
     * not.
     * @return Human friendly description of this instance.
     */
    String humanDescription(boolean verbose);
}
