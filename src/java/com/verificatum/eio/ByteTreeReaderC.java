
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.eio;

/**
 * A reader of {@link ByteTreeContainer} instances.
 *
 * @author Douglas Wikstrom
 */
public final class ByteTreeReaderC extends ByteTreeReader {

    /**
     * Byte tree from which this instance reads.
     */
    ByteTreeContainer btc;

    /**
     * Position of reader within the underlying byte tree.
     */
    int index;

    /**
     * Creates an instance that reads from the given byte tree and
     * with the given parent.
     *
     * @param parent Instance that spawned this one.
     * @param btc Byte tree from which this instance reads.
     */
    protected ByteTreeReaderC(final ByteTreeReader parent,
                              final ByteTreeContainer btc) {
        super(parent, btc.children.length);
        this.btc = btc;
        this.index = 0;
    }

    // Documented in ByteTreeReader.java

    @Override
    public boolean isLeaf() {
        return false;
    }

    @Override
    protected ByteTreeReader getNextChildInner() {
        final ByteTreeReader btr = btc.children[index++].getByteTreeReader();
        btr.parent = this;
        return btr;
    }

    @Override
    protected void readInner(final byte[] destination,
                             final int offset,
                             final int length) {
        throw new EIOError("A ByteTreeContainer is always a node!");
    }

    @Override
    public void close() {
        btc = null;
    }
}
