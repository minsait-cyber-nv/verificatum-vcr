
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.eio;

import java.io.Serializable;
import java.util.Comparator;

/**
 * Comparator of queues of byte trees. Note that this comparator
 * compares the first elements in two queues of byte trees. Note that
 * this comparator imposes orderings that are inconsistent with
 * equals.
 *
 * @author Douglas Wikstrom
 */
public class ByteTreeQueueComparator
    implements Comparator<ByteTreeQueue>, Serializable {

    /**
     * Underlying comparator of byte trees.
     */
    private final ByteTreeComparator comparator;

    /**
     * Underlying comparator of byte trees.
     *
     * @param comparator Comparator of byte trees.
     */
    public ByteTreeQueueComparator(final ByteTreeComparator comparator) {
        this.comparator = comparator;
    }

    @Override
    public int compare(final ByteTreeQueue left, final ByteTreeQueue right) {
        return comparator.compare(left.peekByteTree(), right.peekByteTree());
    }
}
