
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.eio;

/**
 * This interface captures the ability of an instance to convert
 * itself to a {@link ByteTreeBasic}.
 *
 * @author Douglas Wikstrom
 */
public interface ByteTreeConvertible {

    /**
     * Returns a representation of this instance. The output byte tree
     * is only valid as long as this instance is alive. This is
     * important when using classes which requires active deletion.
     *
     * @return Representation of this instance.
     */
    ByteTreeBasic toByteTree();
}
