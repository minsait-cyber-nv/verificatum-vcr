
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.eio;

import java.io.Closeable;

/**
 * Wrapper class that provides a queue-like interface to a byte tree
 * reader.
 *
 * @author Douglas Wikstrom
 */
public class ByteTreeQueue implements Closeable {

    /**
     * Next byte tree to be read.
     */
    private ByteTree head;

    /**
     * Internal reader of byte trees.
     */
    private final ByteTreeReader reader;

    /**
     * Creates a queue from the given byte tree reader.
     *
     * @param reader Underlying byte tree reader.
     * @throws EIOException If reading from the underlying byte tree
     * reader fails.
     */
    public ByteTreeQueue(final ByteTreeReader reader) throws EIOException {
        this.reader = reader;
        if (reader.getRemaining() > 0) {
            this.head = reader.getNextChild().readByteTree();
        } else {
            this.head = null;
        }
    }

    /**
     * Creates a queue from a byte tree reader of the given byte tree.
     *
     * @param byteTree Byte tree from which the underlying byte tree
     * reader reads.
     * @throws EIOException If getting the byte tree reader from the
     * input fails.
     */
    public ByteTreeQueue(final ByteTree byteTree) throws EIOException {
        this(byteTree.getByteTreeReader());
    }

    /**
     * Returns number of elements left in the queue.
     *
     * @return Number of elements left in the queue.
     */
    public int getRemaining() {
        if (head == null) {
            return 0;
        } else {
            return reader.getRemaining() + 1;
        }
    }

    /**
     * Returns the first byte tree in the queue without removing it.
     *
     * @return First byte tree in the queue.
     */
    public ByteTree peekByteTree() {
        return head;
    }

    /**
     * Returns the first byte tree in the queue and removes it.
     *
     * @return First byte tree in the queue.
     * @throws EIOException If reading from the underlying byte tree
     * reader fails.
     */
    public ByteTree popByteTree() throws EIOException {
        final ByteTree result = head;
        if (reader.getRemaining() > 0) {
            head = reader.getNextChild().readByteTree();
        } else {
            head = null;
        }
        return result;
    }

    /**
     * Closes the underlying reader.
     */
    public void close() {
        reader.close();
    }
}

