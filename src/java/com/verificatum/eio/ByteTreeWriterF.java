
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.eio;

import java.io.BufferedOutputStream;
import java.io.Closeable;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;


/**
 * A writer of byte tree instances to file. The functionality of this
 * class does not match that of {@link ByteTreeReader}. This class
 * should be used inside in classes operating on files internally.
 *
 * @author Douglas Wikstrom
 */
public final class ByteTreeWriterF implements Closeable {

    /**
     * Destination of instances.
     */
    DataOutputStream dos;

    /**
     * Creates an instance with the given number of children/bytes to
     * be written.
     *
     * @param remaining Number of children/bytes (supposedly)
     * remaining to be written.
     * @param file Destination of instances.
     *
     * @throws IOException If the output file can not be opened or not
     * written.
     */
    public ByteTreeWriterF(final int remaining, final File file)
        throws IOException {
        final FileOutputStream fos = new FileOutputStream(file);
        final BufferedOutputStream bos = new BufferedOutputStream(fos);
        this.dos = new DataOutputStream(bos);
        dos.writeByte(ByteTreeBasic.NODE);
        dos.writeInt(remaining);
    }

    /**
     * Creates an instance with the given number of children/bytes to
     * be written.
     *
     * @param remaining Number of children/bytes (supposedly)
     * remaining to be written.
     * @param file Destination of instances.
     * @return Byte tree writer.
     *
     * @throws EIOError If the output file can not be opened or not
     * written.
     */
    public static ByteTreeWriterF unsafeByteTreeWriterF(final int remaining,
                                                        final File file)
        throws EIOError {
        try {
            return new ByteTreeWriterF(remaining, file);
        } catch (final FileNotFoundException fnfe) {
            throw new EIOError("Unable to create writer!", fnfe);
        } catch (final IOException ioe) {
            throw new EIOError("Unable to create writer!", ioe);
        }
    }

    /**
     * Writes a byte tree to the underlying file.
     *
     * @param bt Byte tree to be written.
     */
    public void unsafeWrite(final ByteTreeBasic bt) {
        bt.unsafeWriteTo(dos);
    }

    /**
     * Writes a byte tree to the underlying file.
     *
     * @param bt Byte tree to be written.
     * @throws EIOException If writing fails.
     */
    public void write(final ByteTreeBasic bt) throws EIOException {
        bt.writeTo(dos);
    }

    /**
     * Writes byte-tree convertible object to the underlying file.
     *
     * @param btc Byte tree convertible objects to be written.
     * @throws EIOException If writing fails.
     */
    public void write(final ByteTreeConvertible... btc) throws EIOException {
        for (int i = 0; i < btc.length; i++) {
            write(btc[i].toByteTree());
        }
    }

    /**
     * Writes byte-tree convertible object to the underlying file.
     *
     * @param btc Byte tree convertible objects to be written.
     */
    public void unsafeWrite(final ByteTreeConvertible... btc) {
        try {
            write(btc);
        } catch (final EIOException eioe) {
            throw new EIOError("Unable to write!", eioe);
        }
    }

    /**
     * Closes the underlying file.
     */
    public void close() {
        ExtIO.strictClose(dos);
    }
}
