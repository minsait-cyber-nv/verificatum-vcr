
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.test;

import java.io.PrintStream;

/**
 * Container class for global test parameters.
 *
 * @author Douglas Wikstrom
 */
public class TestParameters {

    /**
     * Destination for additional outputs during the test.
     */
    public PrintStream ps;

    /**
     * Seed used for main pseudo-random generator. This is used for
     * debugging.
     */
    public String prgseed;

    /**
     * Maximal execution time for time consuming tests.
     */
    public long milliSeconds;

    /**
     * Create test parameters.
     *
     * @param prgseed Seed used in tests.
     * the test).
     * @param milliSeconds For how long the test proceeds.
     * @param ps Destination for written output of test.
     */
    public TestParameters(final String prgseed,
                          final long milliSeconds,
                          final PrintStream ps) {
        this.prgseed = prgseed;
        this.milliSeconds = milliSeconds;
        this.ps = ps;
    }
}
