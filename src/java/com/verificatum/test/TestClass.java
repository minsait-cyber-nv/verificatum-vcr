
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.test;

import com.verificatum.crypto.PRGHeuristic;
import com.verificatum.crypto.RandomSource;
import com.verificatum.eio.ExtIO;


/**
 * Base class for a class that contains testing methods.
 *
 * @author Douglas Wikstrom
 */
public class TestClass {

    /**
     * Maximal running time of each test.
     */
    protected long testTime;

    /**
     * Test parameters.
     */
    protected TestParameters tp;

    /**
     * Source of randomness.
     */
    protected RandomSource rs;

    /**
     * Constructor needed to avoid that this class is instantiated.
     *
     * @param tp Test parameters.
     */
    protected TestClass(final TestParameters tp) {
        this.testTime = tp.milliSeconds;
        this.rs = new PRGHeuristic(ExtIO.getBytes(tp.prgseed));
        this.tp = tp;
    }
}
