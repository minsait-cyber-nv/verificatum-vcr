
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.tests.ui.gen;

import com.verificatum.arithm.ArithmException;
import com.verificatum.arithm.ArithmFormatException;
import com.verificatum.eio.EIOException;
import com.verificatum.test.TestClass;
import com.verificatum.test.TestParameters;
import com.verificatum.ui.gen.GenException;
import com.verificatum.ui.gen.Generator;

// FB_ANNOTATION import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;


/**
 * Tests {@link com.verificatum.ui.gen.Generator}.
 *
 * @author Douglas Wikstrom
 */
// FB_ANNOTATION @SuppressFBWarnings(value = "RV_RETURN_VALUE_IGNORED")
public abstract class TestGenerator extends TestClass {

    /**
     * Generator.
     */
    protected final Generator generator;

    /**
     * Creates basic testing of generator.
     *
     * @param tp Test parameters.
     * @param generator Generator to be tested.
     */
    public TestGenerator(final TestParameters tp, final Generator generator) {
        super(tp);
        this.generator = generator;
    }

    /**
     * Generator.
     *
     * @throws ArithmException When failing test.
     * @throws ArithmFormatException When failing test.
     * @throws GenException When failing test.
     * @throws EIOException When failing test.
     */
    public void gen()
        throws ArithmException, ArithmFormatException,
               GenException, EIOException {

        // Default case.
        final String[] args = new String[1];
        args[0] = "-h";
        generator.gen(rs, args);
    }

    /**
     * Exercise briefDescription().
     *
     * @throws ArithmException When failing test.
     * @throws ArithmFormatException When failing test.
     */
    public void briefDescription()
        throws ArithmException, ArithmFormatException {
        generator.briefDescription();
    }
}
