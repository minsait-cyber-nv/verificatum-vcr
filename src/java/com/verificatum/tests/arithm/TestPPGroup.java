
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.tests.arithm;

import com.verificatum.arithm.ArithmFormatException;
import com.verificatum.arithm.ECqPGroupParams;
import com.verificatum.arithm.ModPGroup;
import com.verificatum.arithm.PGroup;
import com.verificatum.arithm.PPGroup;
import com.verificatum.crypto.RandomSource;
import com.verificatum.eio.ByteTreeReader;
import com.verificatum.test.TestParameters;


/**
 * Tests {@link PPGroup}.
 *
 * @author Douglas Wikstrom
 */
public class TestPPGroup extends TestPGroup {

    /**
     * Constructs an asymmetric product group of multiplicative
     * groups.
     *
     * @param bitLength Bit length of modulus of basic multiplicative
     * group.
     * @return Group used for testing.
     * @throws ArithmFormatException If construction of the group
     * failed.
     */
    public static PGroup genPGroup(final int bitLength)
        throws ArithmFormatException {
        final PGroup pGroup = new ModPGroup(bitLength);
        return new PPGroup(new PPGroup(pGroup, pGroup), pGroup);
    }

    /**
     * Constructs test.
     *
     * @param tp Test parameters.
     * @throws ArithmFormatException If construction of the test
     * failed.
     */
    public TestPPGroup(final TestParameters tp)
        throws ArithmFormatException {
        super(genPGroup(512),
              genPGroup(640),
              ECqPGroupParams.getECqPGroup("P-256"),
              tp);
    }

    @Override
    protected PGroup[] encodingPGroups()
        throws ArithmFormatException {

        final int bitLength = 512;
        final PGroup pGroup = new ModPGroup(bitLength);

        final PGroup[] pGroups = new PGroup[3];

        pGroups[0] = new PPGroup(new PPGroup(pGroup, pGroup), pGroup);
        pGroups[1] = new PPGroup(new PPGroup(pGroup, pGroup), pGroups[0]);
        pGroups[2] = new PPGroup(pGroups[0], pGroups[1]);

        return pGroups;
    }

    @Override
    protected PGroup newInstance(final ByteTreeReader btr,
                                 final RandomSource rs)
        throws ArithmFormatException {
        return PPGroup.newInstance(btr, rs, 20);
    }
}
