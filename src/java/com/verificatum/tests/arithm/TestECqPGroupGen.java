
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.tests.arithm;

import com.verificatum.arithm.ArithmException;
import com.verificatum.arithm.ArithmFormatException;
import com.verificatum.arithm.ECqPGroupGen;
import com.verificatum.arithm.ECqPGroup;
import com.verificatum.arithm.PGroup;
import com.verificatum.eio.Marshalizer;
import com.verificatum.eio.EIOException;
import com.verificatum.test.TestParameters;
import com.verificatum.ui.gen.GenException;
import com.verificatum.tests.ui.gen.TestGenerator;


/**
 * Tests {@link ECqPGroupGen}.
 *
 * @author Douglas Wikstrom
 */
public class TestECqPGroupGen extends TestGenerator {

    /**
     * Constructs test.
     *
     * @param tp Test parameters.
     */
    public TestECqPGroupGen(final TestParameters tp) {
        super(tp, new ECqPGroupGen());
    }

    @Override
    public void gen()
        throws ArithmException, ArithmFormatException,
               GenException, EIOException {
        super.gen();

        String[] args;

        // Generate group from real group name.
        final String curveName = "P-192";
        args = new String[2];
        args[0] = "-name";
        args[1] = curveName;
        final String groupDescription = generator.gen(rs, args);
        final PGroup pGroup =
            Marshalizer.unmarshalHexAux_PGroup(groupDescription, rs, 20);
        assert curveName.equals(((ECqPGroup) pGroup).getCurveName())
            : "Unable to generate and recover group!";

        // Bad group name.
        boolean invalid = false;
        invalid = false;
        args = new String[2];
        args[0] = "-name";
        args[1] = "xyz";
        try {
            generator.gen(rs, args);
        } catch (final GenException ge) {
            invalid = true;
        }
        assert invalid : "Failed to fail on bad group name!";
    }
}
