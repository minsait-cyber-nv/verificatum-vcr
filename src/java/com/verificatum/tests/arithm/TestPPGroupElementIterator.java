
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.tests.arithm;

import com.verificatum.arithm.ArithmFormatException;
import com.verificatum.arithm.ModPGroup;
import com.verificatum.arithm.PPGroup;
import com.verificatum.arithm.PPGroupElementIterator;
import com.verificatum.test.TestParameters;


/**
 * Tests {@link PPGroupElementIterator}.
 *
 * @author Douglas Wikstrom
 */
public class TestPPGroupElementIterator extends TestPGroupElementIterator {

    /**
     * Constructs test.
     *
     * @param tp Test parameters.
     * @throws ArithmFormatException If construction of the test
     * failed.
     */
    public TestPPGroupElementIterator(final TestParameters tp)
        throws ArithmFormatException {
        super(new PPGroup(new PPGroup(new ModPGroup(512),
                                      new ModPGroup(512)),
                          new ModPGroup(512)), tp);
    }
}
