
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.tests.arithm;

import com.verificatum.arithm.ArithmFormatException;
import com.verificatum.arithm.PGroup;
import com.verificatum.arithm.PGroupElement;
import com.verificatum.arithm.PGroupElementArray;
import com.verificatum.arithm.PGroupElementIterator;
import com.verificatum.test.TestClass;
import com.verificatum.test.TestParameters;
import com.verificatum.util.Timer;


/**
 * Tests {@link PGroupElementIterator}.
 *
 * @author Douglas Wikstrom
 */
public class TestPGroupElementIterator extends TestClass {

    /**
     * Batch used.
     */
    public static final int TEST_BATCH_SIZE = 5;

    /**
     * Group used for testing.
     */
    protected PGroup pGroup;

    /**
     * Constructs test.
     *
     * @param pGroup Group used for testing.
     * @param tp Test parameters.
     * @throws ArithmFormatException If construction of the test
     * failed.
     */
    protected TestPGroupElementIterator(final PGroup pGroup,
                                        final TestParameters tp)
        throws ArithmFormatException {
        super(tp);
        this.pGroup = pGroup;
    }

    /**
     * Iterate.
     */
    protected void iterate() {

        final Timer timer = new Timer(testTime);

        int size = 1;

        // General case.
        while (!timer.timeIsUp()) {

            final PGroupElementArray x =
                pGroup.randomElementArray(size, rs, 10);

            final PGroupElementIterator iterator = x.getIterator();

            final PGroupElement[] elements = x.elements();
            int i = 0;
            while (iterator.hasNext()) {
                final PGroupElement a = iterator.next();
                assert a.equals(elements[i])
                    : "Failed to get the right element!";
                i++;
            }
            assert iterator.next() == null
                : "Failed to indicate end by returning null!";
            iterator.close();
            assert i == x.size() : "Failed to traverse some elements!";

            size++;
        }
    }

    /**
     * Iterate.
     */
    public void iterateIM() {
        TestLargeIntegerArray.memoryBased();
        iterate();
        TestLargeIntegerArray.resetBased();
    }

    /**
     * Iterate.
     */
    public void iterateF() {
        TestLargeIntegerArray.fileBased(TEST_BATCH_SIZE);
        iterate();
        TestLargeIntegerArray.resetBased();
    }
}
