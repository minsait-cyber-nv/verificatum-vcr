
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.tests.arithm;

import com.verificatum.arithm.ArithmFormatException;
import com.verificatum.arithm.LargeInteger;
import com.verificatum.arithm.SafePrimeTable;
import com.verificatum.crypto.PRGHeuristic;
import com.verificatum.crypto.RandomSource;
import com.verificatum.eio.ExtIO;
import com.verificatum.test.TestClass;
import com.verificatum.test.TestParameters;
import com.verificatum.util.Timer;

/**
 * Tests {@link SafePrimeTable}.
 *
 * @author Douglas Wikstrom
 */
public final class TestSafePrimeTable extends TestClass {

    /**
     * Constructs test.
     *
     * @param tp Test parameters.
     */
    public TestSafePrimeTable(final TestParameters tp) {
        super(tp);
    }

    /**
     * Sanity check.
     *
     * @throws ArithmFormatException If a test failed.
     */
    public void sanityCheck()
        throws ArithmFormatException {

        final RandomSource rs = new PRGHeuristic(ExtIO.getBytes(tp.prgseed));
        final Timer timer = new Timer(testTime);

        final int size =
            SafePrimeTable.MAX_BIT_LENGTH - SafePrimeTable.MIN_BIT_LENGTH;

        while (!timer.timeIsUp()) {

            final byte[] indexBytes = rs.getBytes(4);
            final int offset = Math.abs(ExtIO.readInt(indexBytes, 0)) % size;
            final int index = SafePrimeTable.MIN_BIT_LENGTH + offset;

            final LargeInteger candidate = SafePrimeTable.safePrime(index);

            assert candidate.bitLength() == index
                : "Candidate at index " + index + "has wrong bit length!";

            assert candidate.isSafePrime(rs, 5)
                : "Prime of bit length is " + index + " is not safe!";
        }

        boolean invalid = false;
        try {
            SafePrimeTable.safePrime(SafePrimeTable.MIN_BIT_LENGTH - 1);
        } catch (final ArithmFormatException afe) {
            invalid = true;
        }
        assert invalid : "Failed to fail on too small bit length!";

        invalid = false;
        try {
            SafePrimeTable.safePrime(SafePrimeTable.MAX_BIT_LENGTH + 1);
        } catch (final ArithmFormatException afe) {
            invalid = true;
        }
        assert invalid : "Failed to fail on too small bit length!";
    }
}
