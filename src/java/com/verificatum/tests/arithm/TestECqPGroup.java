
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.tests.arithm;

import com.verificatum.arithm.ArithmFormatException;
import com.verificatum.arithm.ECqPGroup;
import com.verificatum.arithm.ECqPGroupParams;
import com.verificatum.arithm.ModPGroup;
import com.verificatum.arithm.PGroup;
import com.verificatum.crypto.RandomSource;
import com.verificatum.eio.ByteTreeReader;
import com.verificatum.test.TestParameters;


/**
 * Tests {@link ECqPGroup}.
 *
 * @author Douglas Wikstrom
 */
public class TestECqPGroup extends TestPGroup {

    /**
     * Constructs test.
     *
     * @param tp Test parameters.
     * @throws ArithmFormatException If construction of the test
     * failed.
     */
    public TestECqPGroup(final TestParameters tp)
        throws ArithmFormatException {
        super(ECqPGroupParams.getECqPGroup("P-256"),
              ECqPGroupParams.getECqPGroup("P-384"),
              new ModPGroup(512),
              tp);
    }

    @Override
    protected PGroup[] encodingPGroups()
        throws ArithmFormatException {

        final PGroup[] pGroups = new PGroup[4];
        pGroups[0] = ECqPGroupParams.getECqPGroup("P-192");
        pGroups[1] = ECqPGroupParams.getECqPGroup("P-256");
        pGroups[2] = ECqPGroupParams.getECqPGroup("P-384");
        pGroups[3] = ECqPGroupParams.getECqPGroup("P-521");

        return pGroups;
    }

    @Override
    protected PGroup newInstance(final ByteTreeReader btr,
                                 final RandomSource rs)
        throws ArithmFormatException {
        return ECqPGroup.newInstance(btr, rs, 20);
    }
}
