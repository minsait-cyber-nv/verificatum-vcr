
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.tests.arithm;

import com.verificatum.arithm.ArithmException;
import com.verificatum.arithm.ArithmFormatException;
import com.verificatum.arithm.LargeInteger;
import com.verificatum.arithm.PGroup;
import com.verificatum.arithm.ModPGroup;
import com.verificatum.arithm.ModPGroupElement;
import com.verificatum.arithm.ModPGroupGen;
import com.verificatum.eio.Marshalizer;
import com.verificatum.eio.EIOException;
import com.verificatum.test.TestParameters;
import com.verificatum.tests.ui.gen.TestGenerator;
import com.verificatum.ui.gen.GenException;

// FB_ANNOTATION import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;


/**
 * Tests {@link ModPGroupGen}.
 *
 * @author Douglas Wikstrom
 */
// FB_ANNOTATION @SuppressFBWarnings(value = "RV_RETURN_VALUE_IGNORED")
public class TestModPGroupGen extends TestGenerator {

    /**
     * Constructs test.
     *
     * @param tp Test parameters.
     */
    public TestModPGroupGen(final TestParameters tp) {
        super(tp, new ModPGroupGen());
    }

    @Override
    public void gen()
        throws ArithmException, ArithmFormatException,
               GenException, EIOException {
        super.gen();

        String groupDescription;
        PGroup pGroup;
        boolean invalid;
        String[] args;

        // Fixed group
        args = new String[2];
        args[0] = "-fixed";
        args[1] = "512";
        groupDescription = generator.gen(rs, args);
        pGroup = Marshalizer.unmarshalHexAux_PGroup(groupDescription, rs, 20);
        assert pGroup.equals(new ModPGroup(512))
            : "Unable to generate and recover fixed group!";

        invalid = false;
        try {
            args[1] = "20";
            groupDescription = generator.gen(rs, args);
        } catch (final GenException ge) {
            invalid = true;
        }
        assert invalid : "Failed to fail on lower bound for fixed groups!";

        invalid = false;
        try {
            args[1] = "20000";
            groupDescription = generator.gen(rs, args);
        } catch (final GenException ge) {
            invalid = true;
        }
        assert invalid : "Failed to fail on upper bound for fixed groups!";


        // Explicit group
        final ModPGroup mpg = new ModPGroup(512);
        final LargeInteger modulus = mpg.getModulus();
        final LargeInteger g = ((ModPGroupElement) mpg.getg()).toLargeInteger();
        final LargeInteger order =
            modulus.sub(LargeInteger.ONE).divide(LargeInteger.TWO);
        mpg.getEncoding();

        args = new String[4];
        args[0] = "-explic";
        args[1] = modulus.toString();
        args[2] = g.toString();
        args[3] = order.toString();
        groupDescription = generator.gen(rs, args);
        pGroup = Marshalizer.unmarshalHexAux_PGroup(groupDescription, rs, 20);
        assert pGroup.equals(mpg)
            : "Unable to generate and recover explicit group!";

        // Random Group
        args = new String[5];
        args[0] = "-rand";
        args[1] = "-roenc";
        args[2] = "-cert";
        args[3] = "20";
        args[4] = "512";
        groupDescription = generator.gen(rs, args);

    }
}
