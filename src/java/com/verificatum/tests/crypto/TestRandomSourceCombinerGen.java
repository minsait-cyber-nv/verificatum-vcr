
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.tests.crypto;

import com.verificatum.arithm.ArithmException;
import com.verificatum.arithm.ArithmFormatException;
import com.verificatum.crypto.RandomDevice;
import com.verificatum.crypto.RandomSource;
import com.verificatum.crypto.RandomSourceCombiner;
import com.verificatum.crypto.RandomSourceCombinerGen;
import com.verificatum.eio.Marshalizer;
import com.verificatum.eio.EIOException;
import com.verificatum.test.TestParameters;
import com.verificatum.tests.ui.gen.TestGenerator;
import com.verificatum.ui.gen.GenException;


/**
 * Tests {@link RandomSourceCombinerGen}.
 *
 * @author Douglas Wikstrom
 */
public class TestRandomSourceCombinerGen extends TestGenerator {

    /**
     * Constructs test.
     *
     * @param tp Test parameters.
     */
    public TestRandomSourceCombinerGen(final TestParameters tp) {
        super(tp, new RandomSourceCombinerGen());
    }

    @Override
    public void gen()
        throws ArithmException, ArithmFormatException,
               GenException, EIOException {
        super.gen();

        final RandomSource atomicSource = new RandomDevice();
        final String atomicSourceString =
            Marshalizer.marshalToHex(atomicSource);
        final RandomSource source =
            new RandomSourceCombiner(atomicSource, atomicSource);

        final String[] args = new String[2];
        args[0] = atomicSourceString;
        args[1] = atomicSourceString;
        final String sourceString = generator.gen(rs, args);
        final RandomSource source2 =
            Marshalizer.unmarshalHex_RandomSource(sourceString);
        assert source2.equals(source)
            : "Unable to generate and recover random source!";
    }
}
