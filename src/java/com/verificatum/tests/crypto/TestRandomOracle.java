
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.tests.crypto;

import java.util.Arrays;

import com.verificatum.crypto.CryptoFormatException;
import com.verificatum.crypto.Hashfunction;
import com.verificatum.crypto.HashfunctionHeuristic;
import com.verificatum.crypto.RandomOracle;
import com.verificatum.eio.ByteTree;
import com.verificatum.eio.ByteTreeBasic;
import com.verificatum.eio.EIOException;
import com.verificatum.test.TestParameters;


/**
 * Tests {@link RandomOracle}.
 *
 * @author Douglas Wikstrom
 */
public final class TestRandomOracle extends TestHashfunction {

    /**
     * Constructor needed to avoid that this class is instantiated.
     *
     * @param tp Test parameters configuration of the servers.
     */
    public TestRandomOracle(final TestParameters tp) {
        super(tp,
              new RandomOracle(new HashfunctionHeuristic("SHA-256"), 700),
              new RandomOracle(new HashfunctionHeuristic("SHA-384"), 700));
    }

    /**
     * newInstance.
     *
     * @throws CryptoFormatException If a test fails.
     */
    public void newInstance()
        throws CryptoFormatException {

        ByteTreeBasic bt = hashfunction.toByteTree();

        final RandomOracle ro =
            RandomOracle.newInstance(bt.getByteTreeReader(), rs, 20);

        final byte[] input = rs.getBytes(100);
        final byte[] output = hashfunction.hash(input);
        final byte[] output2 = ro.hash(input);

        assert Arrays.equals(output, output2) : "Failed to create instance!";

        boolean invalid = false;
        try {
            bt = new ByteTree(new byte[1]);
            RandomOracle.newInstance(bt.getByteTreeReader(), rs, 20);
        } catch (final CryptoFormatException cfe) {
            invalid = true;
        }
        assert invalid : "Failed to fail on bad byte tree!";
    }

    /**
     * Equals.
     *
     * @throws EIOException If the test failed.
     */
    @Override
    public void equality()
        throws EIOException {
        super.equality();

        final Hashfunction hashfunction = new HashfunctionHeuristic("SHA-256");
        final RandomOracle ro1 = new RandomOracle(hashfunction, 150);
        final RandomOracle ro2 = new RandomOracle(hashfunction, 151);

        assert !ro1.equals(ro2) : "Failed to fail on different lengths!";
    }
}

