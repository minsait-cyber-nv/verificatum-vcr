
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.tests.crypto;

import com.verificatum.arithm.ArithmFormatException;
import com.verificatum.arithm.ModPGroup;
import com.verificatum.arithm.PGroup;
import com.verificatum.crypto.CryptoKeyGenNaorYung;
import com.verificatum.crypto.Hashfunction;
import com.verificatum.crypto.HashfunctionHeuristic;
import com.verificatum.test.TestParameters;

// FB_ANNOTATION import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;


/**
 * Tests {@link CryptoKeyGenNaorYung}.
 *
 * @author Douglas Wikstrom
 */
// FB_ANNOTATION @SuppressFBWarnings(value = "RV_RETURN_VALUE_IGNORED")
public final class TestCryptoKeyGenNaorYung extends TestCryptoKeyGen {

    /**
     * Generates a key generator.
     *
     * @param tp Test parameters.
     * @return Key generator.
     * @throws ArithmFormatException If construction of generator fails.
     */
    public static CryptoKeyGenNaorYung keyGen(final TestParameters tp)
        throws ArithmFormatException {
        final int secpro = 256;
        final PGroup pGroup = new ModPGroup(512);
        final Hashfunction roh = new HashfunctionHeuristic("SHA-256");
        return new CryptoKeyGenNaorYung(pGroup, roh, secpro);
    }

    /**
     * Construct test.
     *
     * @param tp Test parameters.
     * @throws ArithmFormatException If construction of the test fails.
     */
    public TestCryptoKeyGenNaorYung(final TestParameters tp)
        throws ArithmFormatException {
        super(tp, keyGen(tp));
    }

    /**
     * Exercise getPGroup.
     */
    public void excGetPGroup() {
        ((CryptoKeyGenNaorYung) keyGen).getPGroup();
    }

    /**
     * Exercise getRandomOracleHashfunction.
     */
    public void excGetRandomOracleHashfunction() {
        ((CryptoKeyGenNaorYung) keyGen).getRandomOracleHashfunction();
    }
}
