
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.tests.crypto;

import java.io.File;

import com.verificatum.crypto.RandomDevice;
import com.verificatum.crypto.RandomSource;
import com.verificatum.eio.ByteTreeBasic;
import com.verificatum.eio.Marshalizer;
import com.verificatum.test.TestParameters;
import com.verificatum.util.Timer;
import com.verificatum.test.TestClass;

// FB_ANNOTATION import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;


/**
 * Tests {@link RandomDevice}.
 *
 * @author Douglas Wikstrom
 */
// PMD_ANNOTATION @SuppressWarnings("PMD.SignatureDeclareThrowsException")
// FB_ANNOTATION @SuppressFBWarnings(value = "RV_RETURN_VALUE_IGNORED")
public final class TestRandomDevice extends TestClass {

    /**
     * Constructs test.
     *
     * @param tp Test parameters.
     */
    public TestRandomDevice(final TestParameters tp) {
        super(tp);
    }

    /**
     * Equals.
     */
    public void equality() {

        final RandomSource urandom =
            new RandomDevice(new File("/dev/urandom"));
        final RandomSource urandomCopy =
            new RandomDevice(new File("/dev/urandom"));
        final RandomSource random =
            new RandomDevice(new File("/dev/random"));

        assert urandom.equals(urandom) : "Equality by reference failed!";
        assert urandomCopy.equals(urandom) : "Equality by value failed!";
        assert !urandom.equals(random) : "Inequality failed!";
    }

    /**
     * Verify that generation works.
     *
     * @throws Exception If a test fails.
     */
    public void generate() throws Exception {

        final RandomSource rs = new RandomDevice();

        final Timer timer = new Timer(testTime);

        int size = 1;

        while (!timer.timeIsUp()) {

            rs.getBytes(size);

            size++;
        }
    }

    /**
     * Verify conversion to byte tree.
     *
     * @throws Exception If a test fails.
     */
    public void marshal() throws Exception {

        final RandomSource rs1 = new RandomDevice();

        final ByteTreeBasic bt = Marshalizer.marshal(rs1);
        final RandomSource rs2 =
            Marshalizer.unmarshal_RandomSource(bt.getByteTreeReader());

        rs1.getBytes(100);
        rs2.getBytes(100);
    }

    /**
     * Exercise human description.
     */
    public void excHumanDescription() {
        new RandomDevice().humanDescription(true);
    }
}
