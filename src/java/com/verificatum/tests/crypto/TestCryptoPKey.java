
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.tests.crypto;

import com.verificatum.arithm.ArithmFormatException;
import com.verificatum.crypto.CryptoKeyGen;
import com.verificatum.crypto.CryptoKeyPair;
import com.verificatum.crypto.CryptoPKey;
import com.verificatum.crypto.CryptoPKeyNaorYung;
import com.verificatum.eio.ByteTreeBasic;
import com.verificatum.eio.ByteTreeReader;
import com.verificatum.eio.EIOException;
import com.verificatum.eio.Marshalizer;
import com.verificatum.test.TestClass;
import com.verificatum.test.TestParameters;

// FB_ANNOTATION import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;


/**
 * Tests {@link CryptoPKey}.
 *
 * @author Douglas Wikstrom
 */
// FB_ANNOTATION @SuppressFBWarnings(value = "RV_RETURN_VALUE_IGNORED")
public class TestCryptoPKey extends TestClass {

    /**
     * Key generator used for testing.
     */
    final CryptoKeyGen keyGen;

    /**
     * Public key used for testing.
     */
    final CryptoPKey pkey;

    /**
     * Construct test.
     *
     * @param tp Test parameters.
     * @param keyGen Key generator.
     * @throws ArithmFormatException If the test cannot be constructed.
     */
    public TestCryptoPKey(final TestParameters tp,
                          final CryptoKeyGen keyGen)
        throws ArithmFormatException {
        super(tp);
        this.keyGen = keyGen;
        final CryptoKeyPair keyPair = keyGen.gen(rs, 10);
        this.pkey = keyPair.getPKey();
    }

    /**
     * Exercise toString.
     */
    public void excToString() {
        pkey.toString();
    }

    /**
     * Exercise humanDescription.
     */
    public void excHumanDescription() {
        pkey.humanDescription(true);
    }

    /**
     * Exercise hashCode.
     */
    public void excHashcode() {
        pkey.hashCode();
    }

    /**
     * Exercise encryption.
     */
    public void excEncryption() {
        final int size = 200;
        final byte[] message = rs.getBytes(size);
        final byte[] label = rs.getBytes(size);
        pkey.encrypt(label, message, rs, 10);
    }

    /**
     * Equals.
     *
     * @throws EIOException If a test fails.
     */
    public void equality()
        throws EIOException {

        final ByteTreeBasic bt = Marshalizer.marshal(pkey);
        final ByteTreeReader btr = bt.getByteTreeReader();
        final CryptoPKeyNaorYung pkeyCopy =
            (CryptoPKeyNaorYung)
            Marshalizer.unmarshalAux_CryptoPKey(btr, rs, 10);

        assert pkey.equals(pkey) : "Equality by reference failed!";
        assert pkey.equals(pkeyCopy) : "Equality by value failed!";

        final CryptoKeyPair keyPair2 = keyGen.gen(rs, 10);
        final CryptoPKey pkey2 = keyPair2.getPKey();

        assert !pkey.equals(pkey2) : "Inequality by value failed!";

        assert !pkey.equals(new Object())
            : "Inequality with instance of different class failed!";
    }
}
