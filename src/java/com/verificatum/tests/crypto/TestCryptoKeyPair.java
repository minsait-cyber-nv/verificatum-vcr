
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.tests.crypto;

import com.verificatum.crypto.CryptoFormatException;
import com.verificatum.crypto.CryptoKeyPair;
import com.verificatum.eio.ByteTree;
import com.verificatum.eio.ByteTreeBasic;
import com.verificatum.eio.ByteTreeReader;
import com.verificatum.test.TestParameters;
import com.verificatum.test.TestClass;

// FB_ANNOTATION import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;


/**
 * Tests {@link CryptoKeyPair}.
 *
 * @author Douglas Wikstrom
 */
// FB_ANNOTATION @SuppressFBWarnings(value = "RV_RETURN_VALUE_IGNORED")
public class TestCryptoKeyPair extends TestClass {

    /**
     * Crypto key pair.
     */
    final CryptoKeyPair keyPair;

    /**
     * Construct test.
     *
     * @param tp Test parameters.
     * @param keyPair Key pair.
     */
    public TestCryptoKeyPair(final TestParameters tp,
                             final CryptoKeyPair keyPair) {
        super(tp);
        this.keyPair = keyPair;
    }

    /**
     * Exercise getPKey.
     */
    public void excGetPKey() {
        keyPair.getPKey();
    }

    /**
     * Exercise getSKey.
     */
    public void excGetSKey() {
        keyPair.getSKey();
    }

    /**
     * Exercise humanDescription.
     */
    public void excHumanDescription() {
        keyPair.humanDescription(true);
    }

    /**
     * Byte tree.
     *
     * @throws CryptoFormatException If the a test fails.
     */
    public void byteTree()
        throws CryptoFormatException {

        final ByteTreeBasic bt = keyPair.toByteTree();

        final ByteTreeReader btr = bt.getByteTreeReader();
        final CryptoKeyPair keyPair2 = CryptoKeyPair.newInstance(btr, rs, 10);

        assert keyPair.getPKey().equals(keyPair2.getPKey())
            : "Public keys are distinct!";
        assert keyPair.getSKey().equals(keyPair2.getSKey())
            : "Secret keys are distinct!";

        final ByteTree btb = new ByteTree(new byte[1]);
        final ByteTreeReader btrb = btb.getByteTreeReader();
        boolean invalid = false;
        try {
            CryptoKeyPair.newInstance(btrb, rs, 10);
        } catch (final CryptoFormatException cfe) {
            invalid = true;
        }
        assert invalid : "Failed to fail on bad byte tree!";
    }
}
