
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.tests.crypto;

import com.verificatum.arithm.ArithmException;
import com.verificatum.arithm.ArithmFormatException;
import com.verificatum.arithm.LargeInteger;
import com.verificatum.arithm.SafePrimeTable;
import com.verificatum.crypto.PRG;
import com.verificatum.crypto.PRGElGamal;
import com.verificatum.crypto.PRGElGamalGen;
import com.verificatum.eio.Marshalizer;
import com.verificatum.eio.EIOException;
import com.verificatum.test.TestParameters;
import com.verificatum.ui.gen.GenException;
import com.verificatum.tests.ui.gen.TestGenerator;


/**
 * Tests {@link PRGElGamalGen}.
 *
 * @author Douglas Wikstrom
 */
public class TestPRGElGamalGen extends TestGenerator {

    /**
     * Constructs test.
     *
     * @param tp Test parameters.
     */
    public TestPRGElGamalGen(final TestParameters tp) {
        super(tp, new PRGElGamalGen());
    }

    @Override
    public void gen()
        throws ArithmException, ArithmFormatException,
               GenException, EIOException {
        super.gen();

        PRG prg = new PRGElGamal(SafePrimeTable.safePrime(512),
                                 PRGElGamal.DEFAULT_STATDIST);
        String[] args;

        // Fixed group
        args = new String[2];
        args[0] = "-fixed";
        args[1] = "512";
        String prgString = generator.gen(rs, args);
        PRG prg2 = Marshalizer.unmarshalHexAux_PRG(prgString, rs, 20);
        assert prg2.equals(prg)
            : "Unable to generate and recover fixed group generator!";

        // Options
        prg = new PRGElGamal(SafePrimeTable.safePrime(512), 3, 20);
        args = new String[8];
        args[0] = "-fixed";
        args[1] = "-statDist";
        args[2] = "20";
        args[3] = "-width";
        args[4] = "3";
        args[5] = "-cert";
        args[6] = "30";
        args[7] = "512";
        prgString = generator.gen(rs, args);
        prg2 = Marshalizer.unmarshalHexAux_PRG(prgString, rs, 20);
        assert prg2.equals(prg)
            : "Unable to generate and recover fixed group generator!";

        // Fixed group
        final LargeInteger prime = SafePrimeTable.safePrime(512);
        prg = new PRGElGamal(prime, PRGElGamal.DEFAULT_STATDIST);
        args = new String[2];
        args[0] = "-explic";
        args[1] = prime.toString();
        prgString = generator.gen(rs, args);
        prg2 = Marshalizer.unmarshalHexAux_PRG(prgString, rs, 20);
        assert prg2.equals(prg)
            : "Unable to generate and recover explicit group generator!";
    }
}
