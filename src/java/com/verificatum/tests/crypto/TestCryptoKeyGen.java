
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.tests.crypto;

import com.verificatum.arithm.ArithmFormatException;
import com.verificatum.crypto.CryptoKeyGen;
import com.verificatum.eio.ByteTreeBasic;
import com.verificatum.eio.ByteTreeReader;
import com.verificatum.eio.EIOException;
import com.verificatum.eio.Marshalizer;
import com.verificatum.test.TestParameters;
import com.verificatum.test.TestClass;

// FB_ANNOTATION import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

/**
 * Tests {@link CryptoKeyGen}.
 *
 * @author Douglas Wikstrom
 */
// FB_ANNOTATION @SuppressFBWarnings(value = "RV_RETURN_VALUE_IGNORED")
public class TestCryptoKeyGen extends TestClass {

    /**
     * Key generator used for testing.
     */
    final CryptoKeyGen keyGen;

    /**
     * Construct test.
     *
     * @param tp Test parameters.
     * @param keyGen Key generator used for testing.
     * @throws ArithmFormatException If construction of the test fails.
     */
    public TestCryptoKeyGen(final TestParameters tp,
                            final CryptoKeyGen keyGen)
        throws ArithmFormatException {
        super(tp);
        this.keyGen = keyGen;
    }

    /**
     * Marshalize key generator.
     *
     * @throws EIOException If a test fails.
     */
    public void equalsAndMarshal()
        throws EIOException {

        assert keyGen.equals(keyGen)
            : "Equality by reference failed!";

        assert !keyGen.equals(new Object())
            : "Inequality with instance of wrong class failed!";

        final ByteTreeBasic keyGenBT = Marshalizer.marshal(keyGen);
        final ByteTreeReader reader = keyGenBT.getByteTreeReader();

        final CryptoKeyGen keyGen2 =
            (CryptoKeyGen)
            Marshalizer.unmarshalAux_CryptoKeyGen(reader, rs, 10);

        assert keyGen2.equals(keyGen)
            : "Failed to marshal or equality by value failed!";
    }

    /**
     * Exercise toString.
     */
    public void excToString() {
        keyGen.toString();
    }

    /**
     * Exercise humanDescription.
     */
    public void excHumanDescription() {
        keyGen.humanDescription(true);
    }

    /**
     * Exercise hashCode.
     */
    public void excHashCode() {
        keyGen.hashCode();
    }
}
