
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.tests.crypto;

import com.verificatum.arithm.ArithmFormatException;
import com.verificatum.crypto.CryptoKeyGenNaorYung;
import com.verificatum.crypto.CryptoKeyPair;
import com.verificatum.crypto.PRGHeuristic;
import com.verificatum.crypto.RandomSource;
import com.verificatum.eio.ExtIO;
import com.verificatum.test.TestParameters;


/**
 * Tests {@link CryptoKeyPair} for Naor-Yung keys.
 *
 * @author Douglas Wikstrom
 */
public final class TestCryptoKeyPairNaorYung extends TestCryptoKeyPair {

    /**
     * Generates key pair used for testing.
     *
     * @param tp Test parameters.
     * @return Key pair used for testing.
     * @throws ArithmFormatException If a test fails.
     */
    public static CryptoKeyPair keyPair(final TestParameters tp)
        throws ArithmFormatException {
        final CryptoKeyGenNaorYung keyGen =
            TestCryptoKeyGenNaorYung.keyGen(tp);
        final RandomSource rs = new PRGHeuristic(ExtIO.getBytes(tp.prgseed));
        return keyGen.gen(rs, 10);
    }

    /**
     * Construct test.
     *
     * @param tp Test parameters.
     * @throws ArithmFormatException If a test fails.
     */
    public TestCryptoKeyPairNaorYung(final TestParameters tp)
        throws ArithmFormatException {
        super(tp, keyPair(tp));
    }
}
