
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.tests.crypto;

import com.verificatum.crypto.CryptoPKey;
import com.verificatum.crypto.CryptoPKeyTrivial;
import com.verificatum.test.TestClass;
import com.verificatum.test.TestParameters;


/**
 * Tests {@link CryptoPKeyTrivial}.
 *
 * @author Douglas Wikstrom
 */
public final class TestCryptoPKeyTrivial extends TestClass {

    /**
     * Trivial public key for use.
     */
    private final CryptoPKey pKey;

    /**
     * Constructor needed to avoid that this class is instantiated.
     *
     * @param tp Test parameters.
     */
    public TestCryptoPKeyTrivial(final TestParameters tp) {
        super(tp);
        this.pKey = new CryptoPKeyTrivial();
    }

    /**
     * Exercise encryption.
     */
    public void excEncryption() {

        final byte[] label = new byte[1];
        final byte[] message = new byte[1];

        pKey.encrypt(label, message, rs, 10);
    }

    /**
     * Excercise byte tree.
     */
    public void excByteTree() {
        pKey.toByteTree();
    }

    /**
     * Exercise humanDescription.
     */
    public void excHumanDescription() {
        pKey.humanDescription(true);
    }
}
