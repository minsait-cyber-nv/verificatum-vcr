
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.vcr;

/**
 * Global utility functions that has to do with any Verificatum
 * software.
 *
 * @author Douglas Wikstrom
 */
public final class VCR {

    /**
     * Avoid accidental instantiation.
     */
    private VCR() { }

    /**
     * Avoid accidental instantiation.
     *
     * @return Version of this library.
     */
    public static String version() {
        return VCR.class.getPackage().getSpecificationVersion();
    }

    /**
     * Returns the major version number as an integer.
     *
     * @return Major version number
     */
    public static int major() {
        return Integer.parseInt(version().split("\\.")[0]);
    }

    /**
     * Returns the minor version number as an integer.
     *
     * @return Minor version number
     */
    public static int minor() {
        return Integer.parseInt(version().split("\\.")[1]);
    }

    /**
     * Returns the revision version number as an integer.
     *
     * @return Revision version number
     */
    public static int revision() {
        return Integer.parseInt(version().split("\\.")[2]);
    }
}
