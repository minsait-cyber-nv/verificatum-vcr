
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.protocol.com;

/**
 * A simple sleeping thread that sleeps at for a given amount of time
 * unless interrupted prematurely.
 *
 * @author Douglas Wikstrom
 */
public final class Sleeper extends Thread {

    /**
     * Maximum time this instance sleeps when executed as a thread.
     */
    long waitTime;

    /**
     * Creates an instance that waits for at most the given amount of
     * time when started as a thread.
     *
     * @param waitTime Maximum time this instance sleeps when executed
     * as a thread.
     */
    Sleeper(final long waitTime) {
        this.waitTime = waitTime;
    }

    // Documented in Thread.java

    @Override
    public void run() {
        try {
            Thread.sleep(waitTime);
        } catch (final InterruptedException ie) {
        }
    }
}
