
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.protocol.com;

import com.verificatum.crypto.RandomSource;
import com.verificatum.ui.info.PartyInfo;
import com.verificatum.ui.info.PrivateInfo;
import com.verificatum.ui.info.ProtocolInfo;

/**
 * Defines which information is stored in the protocol and private
 * info files, and also defines default values of some fields. For
 * each subclass of {@link BullBoardBasic}, there should be a
 * corresponding subclass of this class that allows adding the needed
 * fields and default values.
 *
 * @author Douglas Wikstrom
 */
public class BullBoardBasicGen {

    /**
     * Add additional fields to the protocol info.
     *
     * @param pri Destination of new fields.
     */
    public void addProtocolInfo(final ProtocolInfo pri) {
    }

    /**
     * Add additional values to the protocol info.
     *
     * @param pri Destination of the values.
     */
    public void addDefault(final ProtocolInfo pri) {
    }

    /**
     * Add additional fields to the private info.
     *
     * @param pi Destination of new fields.
     */
    public void addPrivateInfo(final PrivateInfo pi) {
    }

    /**
     * Add additional values to the private info.
     *
     * @param pi Destination of values.
     * @param pri Associated protocol info.
     * @param rs Source of randomness.
     */
    public void addDefault(final PrivateInfo pi,
                           final ProtocolInfo pri,
                           final RandomSource rs) {
    }

    /**
     * Add additional values to the party info.
     *
     * @param pai Destination of values.
     * @param pri Associated protocol info.
     * @param pi Associated private info.
     * @param rs Source of randomness.
     */
    public void addDefault(final PartyInfo pai,
                           final ProtocolInfo pri,
                           final PrivateInfo pi,
                           final RandomSource rs) {
    }
}
