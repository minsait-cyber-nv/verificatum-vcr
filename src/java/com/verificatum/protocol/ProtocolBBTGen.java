
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.protocol;

import com.verificatum.protocol.com.BullBoardBasicGen;
import com.verificatum.ui.info.InfoException;
import com.verificatum.ui.info.IntField;
import com.verificatum.ui.info.ProtocolInfo;

/**
 * Defines additional fields and default values used by
 * {@link ProtocolBB}.
 *
 * @author Douglas Wikstrom
 */
public class ProtocolBBTGen extends ProtocolBBGen {

    /**
     * Creates an instance for a given implementation of a bulletin
     * board.
     *
     * @param bbbg Adds the values needed by the particular
     * instantiation of bulletin board used.
     */
    public ProtocolBBTGen(final BullBoardBasicGen bbbg) {
        super(bbbg);
    }

    /**
     * Creates an instance for the default bulletin board.
     */
    public ProtocolBBTGen() {
        super();
    }

    /**
     * Threshold number of parties needed to violate privacy, i.e.,
     * this is the number of parties needed to decrypt.
     */
    public static final String THRESHOLD_DESCRIPTION =
        "Threshold number of parties needed to violate the privacy of the "
        + "protocol, i.e., this is the number of parties needed to decrypt. "
        + "This must be positive, but at most equal to the number of parties.";

    @Override
    public void addProtocolInfo(final ProtocolInfo pri) {
        super.addProtocolInfo(pri);

        final IntField thresField =
            new IntField(ProtocolBBT.THRESHOLD,
                         THRESHOLD_DESCRIPTION, 1, 1, 1,
                         ProtocolGen.MAX_NOPARTIES);
        pri.addInfoFields(thresField);
    }

    // There is no default number for the threshold.

    @Override
    public void validateLocal(final ProtocolInfo pri) throws InfoException {
        super.validateLocal(pri);
        final int noParties = pri.getIntValue(Protocol.NOPARTIES);
        final int threshold = pri.getIntValue(ProtocolBBT.THRESHOLD);
        if (threshold > noParties) {
            throw new InfoException("Threshold is greater than number of "
                                    + "parties! (" + threshold + " > "
                                    + noParties + ")");
        }
    }
}
