
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.protocol;

import com.verificatum.eio.ByteTree;
import com.verificatum.eio.ByteTreeReader;
import com.verificatum.ui.Log;

/**
 * The trivial protocol where each party generates a key pair of a
 * cryptosystem and then shares it with the other parties.
 *
 * @author Douglas Wikstrom
 */
public final class Shutdown extends ProtocolBB {

    /**
     * Creates an instance of the protocol.
     *
     * @param sid Session identifier of this instance.
     * @param protocol Protocol which invokes this one.
     */
    public Shutdown(final String sid, final ProtocolBB protocol) {
        super(sid, protocol);
    }

    /**
     * Shut down the protocol in a synchronized way.
     *
     * @param log Logging context.
     */
    public void execute(final Log log) {

        final String label = "shutdown";

        log.info("Waiting for mutual shutdown acknowledgements.");

        final Log tempLog = log.newChildLog();

        executeInner(tempLog, label + "_first_round");
        executeInner(tempLog, label + "_second_round");

        log.info("Allow download of our acknowledgment for another "
                 + (WAIT_FOR_OTHERS_TIME / 1000) + " seconds.");

        try {
            Thread.sleep(WAIT_FOR_OTHERS_TIME);
        } catch (final InterruptedException ie) {
            // User requested immediate shutdown. Do nothing.
        }
        bullBoard.stop(log);
    }

    /**
     * Execute one round of synchronized shut down.
     *
     * @param log Logging context.
     * @param label Unique label among invocations of this class.
     */
    public void executeInner(final Log log, final String label) {

        for (int i = 1; i <= k; i++) {

            if (getActive(i)) {

                if (i == j) {

                    log.info("Publish acknowledgement.");
                    bullBoard.publish(label, new ByteTree(), log);

                } else {

                    log.info("Wait for " + ui.getDescrString(i)
                             + " to acknowledge.");
                    final ByteTreeReader btr =
                        bullBoard.waitFor(i, label, log);
                    btr.close();
                }
            }
        }
    }
}
