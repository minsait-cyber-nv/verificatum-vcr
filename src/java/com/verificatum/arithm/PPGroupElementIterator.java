
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.arithm;

import java.util.Arrays;

/**
 * Iterator over a {@link PPGroupElementArray}.
 *
 * @author Douglas Wikstrom
 */
public final class PPGroupElementIterator implements PGroupElementIterator {

    /**
     * Underlying group.
     */
    PPGroup pPGroup;

    /**
     * Underlying iterators.
     */
    PGroupElementIterator[] iterators;

    /**
     * Creates an instance over a {@link PPGroupElementArray}.
     *
     * @param pPGroup Underlying group.
     * @param iterators Underlying iterators.
     */
    public PPGroupElementIterator(final PPGroup pPGroup,
                                  final PGroupElementIterator[] iterators) {
        this.pPGroup = pPGroup;
        this.iterators = Arrays.copyOf(iterators, iterators.length);
    }

    // Documented in PGroupElementIterator.java

    @Override
    public PGroupElement next() {
        final PGroupElement[] res = new PGroupElement[iterators.length];
        for (int i = 0; i < res.length; i++) {
            res[i] = iterators[i].next();
        }
        if (res[0] == null) {
            return null;
        } else {
            return pPGroup.product(res);
        }
    }

    @Override
    public boolean hasNext() {
        boolean res = true;
        for (int i = 0; i < iterators.length; i++) {
            if (!iterators[i].hasNext()) {
                res = false;
            }
        }
        return res;
    }

    @Override
    public void close() {
        for (int i = 0; i < iterators.length; i++) {
            iterators[i].close();
        }
    }
}
