
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.arithm;

import java.util.Arrays;

import com.verificatum.eio.ByteTreeBasic;
import com.verificatum.eio.ByteTreeContainer;


/**
 * Product homomorphism.
 *
 * @author Douglas Wikstrom
 */
public class PHomPRingPGroup implements HomPRingPGroup {

    /**
     * Product of the underlying domains.
     */
    protected PPRing domain;

    /**
     * Product of the underlying ranges.
     */
    protected PPGroup range;

    /**
     * Underlying homomorphisms.
     */
    HomPRingPGroup[] homs;

    /**
     * Creates the product homomorphism of the input homomorphisms.
     *
     * @param homs Underlying homomorphisms.
     */
    public PHomPRingPGroup(final HomPRingPGroup... homs) {
        this.homs = homs;

        final PRing[] pRings = new PRing[homs.length];
        for (int i = 0; i < pRings.length; i++) {
            pRings[i] = homs[i].getDomain();
        }
        this.domain = new PPRing(pRings);

        final PGroup[] pGroups = new PGroup[homs.length];
        for (int i = 0; i < pGroups.length; i++) {
            pGroups[i] = homs[i].getRange();
        }
        this.range = new PPGroup(pGroups);
    }

    /**
     * Returns an array of the underlying arrays.
     *
     * @return Array of underlying homomorphisms.
     */
    public HomPRingPGroup[] getFactors() {
        return Arrays.copyOfRange(homs, 0, homs.length);
    }

    // Documented in HomPRingPGroup.java

    @Override
    public PRing getDomain() {
        return domain;
    }

    @Override
    public PGroup getRange() {
        return range;
    }

    @Override
    public PGroupElement map(final PRingElement element) {

        if (domain.contains(element)) {

            final PRingElement[] elements =
                ((PPRingElement) element).getFactors();

            final PGroupElement[] res = new PGroupElement[elements.length];
            for (int i = 0; i < res.length; i++) {
                res[i] = homs[i].map(elements[i]);
            }
            return range.product(res);
        }
        throw new ArithmError("Element not in domain!");
    }

    @Override
    public ByteTreeBasic toByteTree() {
        final ByteTreeBasic[] bts = new ByteTreeBasic[homs.length];

        for (int i = 0; i < bts.length; i++) {
            bts[i] = homs[i].toByteTree();
        }

        return new ByteTreeContainer(homs);
    }
}
