
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.arithm;

/**
 * Bilinear map capturing exponentiated products. The multiple group
 * elements and exponents are represented by {@link PPGroupElement}
 * and {@link PPRingElement} instances that must contain the same
 * number of underlying elements and with matching groups and rings.
 *
 * @author Douglas Wikstrom
 */
public final class BiExpProd extends BiPRingPGroup {

    /**
     * Underlying group.
     */
    private final PGroup pGroup;

    /**
     * Group domain of map.
     */
    private final PGroup pGroupDomain;

    /**
     * Creates an instance with the given underlying group.
     *
     * @param pGroup Underlying group.
     * @param width Number of bases in exponentiated product.
     */
    public BiExpProd(final PGroup pGroup, final int width) {
        this.pGroup = pGroup;
        this.pGroupDomain = new PPGroup(pGroup, width);
    }

    // Documented in BiPRingPGroup.java

    @Override
    public PRing getPRingDomain() {
        return pGroupDomain.getPRing();
    }

    @Override
    public PGroup getPGroupDomain() {
        return pGroupDomain;
    }

    @Override
    public PGroup getRange() {
        return pGroup;
    }

    @Override
    public PGroupElement map(final PRingElement ringElement,
                             final PGroupElement groupElement) {

        if (!groupElement.getPGroup().equals(pGroupDomain)
            || !ringElement.getPRing().equals(pGroupDomain.getPRing())) {

            throw new ArithmError("Inputs not in domains!");
        }

        final PRingElement[] ringFactors =
            ((PPRingElement) ringElement).getFactors();
        final PGroupElement[] groupFactors =
            ((PPGroupElement) groupElement).getFactors();

        PGroupElement res = pGroup.getONE();
        for (int i = 0; i < groupFactors.length; i++) {
            res = res.mul(groupFactors[i].exp(ringFactors[i]));
        }
        return res;
    }
}
