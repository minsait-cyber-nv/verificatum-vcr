
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.arithm;

import com.verificatum.eio.ByteTreeWriterF;

/**
 * Writes a batch of group elements in a separate thread. This allows
 * making better use of multiple cores.
 *
 * @author Douglas Wikstrom
 */
public class BPGroupElementBatchWriter {

    /**
     * Destination of group elements.
     */
    ByteTreeWriterF btw;

    /**
     * Indicates if this instance is still writing.
     */
    boolean active;

    /**
     * Creates a group element writer.
     *
     * @param btw Destination of group elements.
     */
    BPGroupElementBatchWriter(final ByteTreeWriterF btw) {
        this.btw = btw;
        this.active = false;
    }

    /**
     * Write the next batch of group element to this writer.
     *
     * @param batch Next batch of group elements.
     */
    void writeNext(final PGroupElement[] batch) {

        while (active) {
            try {
                Thread.sleep(100);
            } catch (final InterruptedException ie) {
            }
        }

        active = true;

        final Thread thread = new Thread() {

                @Override
                public void run() {
                    btw.unsafeWrite(batch);
                    active = false;
                }
            };
        thread.start();
    }

    /**
     * Release allocated resources. This returns only after all
     * elements in the last write operation have been written.
     */
    void close() {
        while (active) {
            try {
                Thread.sleep(100);
            } catch (final InterruptedException ie) {
            }
        }
        btw.close();
    }
}
