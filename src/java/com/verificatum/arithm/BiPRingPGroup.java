
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.arithm;

/**
 * Abstract class for a bilinear map from the product of a
 * {@link PRing} and {@link PGroup} to a {@link PGroup}.
 *
 * @author Douglas Wikstrom
 */
public abstract class BiPRingPGroup {

    /**
     * Returns the ring part of the domain of this map.
     *
     * @return Ring part of the domain of this map.
     */
    public abstract PRing getPRingDomain();

    /**
     * Returns the group part of the domain of this map.
     *
     * @return Group part of the domain of this map.
     */
    public abstract PGroup getPGroupDomain();

    /**
     * Returns the range of this map.
     *
     * @return Range of this map.
     */
    public abstract PGroup getRange();

    /**
     * Evaluates the map at the given point.
     *
     * @param ringElement Ring element input.
     * @param groupElement Group element input.
     * @return Value of the map at the given point.
     */
    public abstract PGroupElement map(PRingElement ringElement,
                                      PGroupElement groupElement);

    /**
     * Returns the homomorphism resulting from restricting this
     * bilinear map.
     *
     * @param groupElement Restriction of this bilinear map.
     * @return Resulting homomorphism.
     */
    public HomPRingPGroup restrict(final PGroupElement groupElement) {
        return new HomPRingPGroupRest(this, groupElement);
    }

    /**
     * Returns the homomorphism resulting from restricting this
     * bilinear map.
     *
     * @param ringElement Restriction of this bilinear map.
     * @return Resulting homomorphism.
     */
    public HomPGroupPGroup restrict(final PRingElement ringElement) {
        return new HomPGroupPGroupRest(this, ringElement);
    }
}
