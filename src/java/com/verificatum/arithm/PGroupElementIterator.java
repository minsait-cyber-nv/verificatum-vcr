
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.arithm;

/**
 * Interface for an iterator over a {@link PGroupElementArray}. All
 * elements must be traversed to let the underlying implementations
 * deallocate resources.
 *
 * @author Douglas Wikstrom
 */
public interface PGroupElementIterator {

    /**
     * Returns the next group element, or null if no more elements are
     * available.
     *
     * @return Next group element.
     */
    PGroupElement next();

    /**
     * Returns true if and only if there is another element in the
     * iterator.
     *
     * @return True or false depending on if there is another element
     * in the iterator.
     */
    boolean hasNext();

    /**
     * Closes this iterator and any potential underlying sources of
     * elements.
     */
    void close();
}
