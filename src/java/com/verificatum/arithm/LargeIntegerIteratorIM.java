
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.arithm;

/**
 * Interface for an iterator over a {@link LargeIntegerArray}.
 *
 * @author Douglas Wikstrom
 */
public final class LargeIntegerIteratorIM implements LargeIntegerIterator {

    /**
     * Current index.
     */
    private int current;

    /**
     * Underlying array.
     */
    final LargeIntegerArrayIM array;

    /**
     * Creates an iterator reading from the given array.
     *
     * @param array Underlying array.
     */
    public LargeIntegerIteratorIM(final LargeIntegerArrayIM array) {
        this.array = array;
        this.current = 0;
    }

    // Documented in LargeIntegerIterator.java

    @Override
    public LargeInteger next() {
        if (current < array.li.length) {
            return array.li[current++];
        } else {
            return null;
        }
    }

    @Override
    public boolean hasNext() {
        return current < array.li.length;
    }

    /**
     * This method is needed to give a uniform interface for both
     * memory mapped and file mapped iterators. The latter must be
     * closed manually.
     */
    @Override
    public void close() {
    }
}
