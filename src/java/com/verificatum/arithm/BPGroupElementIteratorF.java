
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.arithm;

import java.io.File;

import com.verificatum.eio.ByteTreeF;
import com.verificatum.eio.ByteTreeReader;
import com.verificatum.eio.EIOException;


/**
 * Iterator over a {@link BPGroupElementArrayF}.
 *
 * @author Douglas Wikstrom
 */
public class BPGroupElementIteratorF implements PGroupElementIterator {

    /**
     * Group to which the elements of the array belongs.
     */
    protected PGroup pGroup;

    /**
     * Underlying source of elements.
     */
    protected ByteTreeReader btr;

    /**
     * Creates an iterator reading from the given array.
     *
     * @param pGroup Group to which the elements of this array
     * belongs.
     * @param file Underlying file
     */
    public BPGroupElementIteratorF(final PGroup pGroup, final File file) {
        this.pGroup = pGroup;
        this.btr = new ByteTreeF(file).getByteTreeReader();
    }

    // Documented in PGroupElementIterator.java

    @Override
    public PGroupElement next() {
        if (btr.getRemaining() > 0) {
            try {
                return pGroup.toElement(btr.getNextChild());
            } catch (final EIOException eioe) {
                throw new ArithmError("Unable to read element!", eioe);
            } catch (final ArithmFormatException afe) {
                throw new ArithmError("Unable to read element!", afe);
            }
        } else {
            return null;
        }
    }

    @Override
    public boolean hasNext() {
        return btr.getRemaining() > 0;
    }

    @Override
    public void close() {
        btr.close();
    }
}
