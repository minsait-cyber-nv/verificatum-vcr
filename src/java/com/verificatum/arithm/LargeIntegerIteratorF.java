
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.arithm;

import java.io.File;

import com.verificatum.eio.ByteTreeF;
import com.verificatum.eio.ByteTreeReader;
import com.verificatum.eio.EIOException;


/**
 * Interface for an iterator over a {@link LargeIntegerArray}.
 *
 * @author Douglas Wikstrom
 */
public final class LargeIntegerIteratorF implements LargeIntegerIterator {

    /**
     * Underlying source of elements.
     */
    private final ByteTreeReader btr;

    /**
     * Creates an iterator reading from the given array.
     *
     * @param file Underlying file
     */
    public LargeIntegerIteratorF(final File file) {
        this.btr = new ByteTreeF(file).getByteTreeReader();
    }

    // Documented in LargeIntegerIterator.java

    @Override
    public LargeInteger next() {
        if (btr.getRemaining() > 0) {
            try {
                return new LargeInteger(btr.getNextChild());
            } catch (final ArithmFormatException afe) {
                throw new ArithmError("Unable to read integer!", afe);

            // UNCOVERABLE (We are guaranteed there are children.)
            } catch (final EIOException eioe) {
                throw new ArithmError("Unable to read integer!", eioe);
            }
        } else {
            return null;
        }
    }

    @Override
    public boolean hasNext() {
        return btr.getRemaining() > 0;
    }

    @Override
    public void close() {
        btr.close();
    }
}
