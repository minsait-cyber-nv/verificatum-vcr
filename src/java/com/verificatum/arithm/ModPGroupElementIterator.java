
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.arithm;

/**
 * Iterator over a {@link ModPGroupElementArray}.
 *
 * @author Douglas Wikstrom
 */
public final class ModPGroupElementIterator implements PGroupElementIterator {

    /**
     * Underlying group.
     */
    ModPGroup modPGroup;

    /**
     * Underlying iterator over integers.
     */
    LargeIntegerIterator integerIterator;

    /**
     * Creates an instance over a {@link ModPGroupElementArray}.
     *
     * @param modPGroup Underlying group.
     * @param integerIterator Underlying integer iterator.
     */
    public
        ModPGroupElementIterator(final ModPGroup modPGroup,
                                 final LargeIntegerIterator integerIterator) {
        this.modPGroup = modPGroup;
        this.integerIterator = integerIterator;
    }

    // Documented in PGroupElementIterator.java

    @Override
    public PGroupElement next() {
        final LargeInteger integer = integerIterator.next();
        if (integer == null) {
            return null;
        } else {
            return modPGroup.toElement(integer);
        }
    }

    @Override
    public boolean hasNext() {
        return integerIterator.hasNext();
    }

    @Override
    public void close() {
        integerIterator.close();
    }
}
