
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.arithm;

/**
 * Iterator over a {@link BPGroupElementArrayIM}.
 *
 * @author Douglas Wikstrom
 */
public class BPGroupElementIteratorIM implements PGroupElementIterator {

    /**
     * Underlying array.
     */
    protected BPGroupElementArrayIM array;

    /**
     * Index of current element.
     */
    protected int current;

    /**
     * Creates an instance over a {@link BPGroupElementArrayIM}.
     *
     * @param array Underlying array.
     */
    public BPGroupElementIteratorIM(final BPGroupElementArrayIM array) {
        this.array = array;
        this.current = 0;
    }

    // Documented in PGroupElementIterator.java

    @Override
    public PGroupElement next() {
        if (current < array.size()) {
            return this.array.values[current++];
        } else {
            return null;
        }
    }

    @Override
    public boolean hasNext() {
        return current < array.size();
    }

    @Override
    public void close() {
    }
}
