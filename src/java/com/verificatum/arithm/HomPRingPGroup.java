
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.arithm;

import com.verificatum.eio.ByteTreeConvertible;

/**
 * Interface for a homomorphism from a {@link PRing} to a
 * {@link PGroup}.
 *
 * @author Douglas Wikstrom
 */
public interface HomPRingPGroup extends ByteTreeConvertible {

    /**
     * Returns the domain of this homomorphism.
     *
     * @return Domain of this homomorphism.
     */
    PRing getDomain();

    /**
     * Returns the range of this homomorphism.
     *
     * @return Range of this homomorphism.
     */
    PGroup getRange();

    /**
     * Evaluates the homomorphism at the given point.
     *
     * @param element Point of evaluation.
     * @return Value of the homomorphism at the given point.
     */
    PGroupElement map(PRingElement element);
}
