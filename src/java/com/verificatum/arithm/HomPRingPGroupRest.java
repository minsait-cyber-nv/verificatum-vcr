
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.arithm;

import com.verificatum.eio.ByteTreeBasic;

/**
 * Restriction of a bilinear map to a homomorphism.
 *
 * @author Douglas Wikstrom
 */
public final class HomPRingPGroupRest implements HomPRingPGroup {

    /**
     * Underlying bilinear map.
     */
    final BiPRingPGroup bi;

    /**
     * Restriction inducing a homomorphism.
     */
    final PGroupElement restriction;

    /**
     * Creates the homomorphism from the given restriction.
     *
     * @param bi Underlying bilinear map.
     * @param restriction Restriction of the bilinear map.
     */
    public HomPRingPGroupRest(final BiPRingPGroup bi,
                              final PGroupElement restriction) {
        this.bi = bi;
        this.restriction = restriction;
    }

    // Documented in HomPRingPGroup.java

    @Override
    public PRing getDomain() {
        return bi.getPRingDomain();
    }

    @Override
    public PGroup getRange() {
        return bi.getRange();
    }

    @Override
    public PGroupElement map(final PRingElement element) {
        return bi.map(element, restriction);
    }

    @Override
    public ByteTreeBasic toByteTree() {
        return restriction.toByteTree();
    }
}
