
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.arithm;

/**
 * Mathematical utility functions for primitive types.
 *
 * @author Douglas Wikstrom
 */
public final class MathExt {

    /**
     * Bit-mask.
     */
    public static final int MASK = 0x80000000;

    /**
     * Maximal bit length of the input.
     */
    public static final int MAX_BITLENGTH = 32;

    /**
     * This prevents instantiation.
     */
    private MathExt() { }

    /**
     * Returns the least upper bound of the binary logarithm of the
     * input, which is assumed to be positive. The output is undefined
     * otherwise.
     *
     * @param x Integer of which the logarithmic upper bound is
     * requested.
     * @return Least upper bound on the binary logarithm of the input.
     */
    public static int log2c(final int x) {
        int mask = MASK;
        int res = MAX_BITLENGTH;
        while ((x & mask) == 0 && res >= 1) {
            mask >>>= 1;
            res--;
        }
        return res;
    }
}
