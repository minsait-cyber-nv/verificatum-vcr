
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.arithm;

/**
 * Bilinear map capturing exponentiation.
 *
 * @author Douglas Wikstrom
 */
public final class BiExp extends BiPRingPGroup {

    /**
     * Underlying group.
     */
    private final PGroup pGroup;

    /**
     * Creates an instance with the given underlying group.
     *
     * @param pGroup Underlying group.
     */
    public BiExp(final PGroup pGroup) {
        this.pGroup = pGroup;
    }

    // Documented in BiPRingPGroup.java

    @Override
    public PRing getPRingDomain() {
        return pGroup.getPRing();
    }

    @Override
    public PGroup getPGroupDomain() {
        return pGroup;
    }

    @Override
    public PGroup getRange() {
        return pGroup;
    }

    @Override
    public PGroupElement map(final PRingElement ringElement,
                             final PGroupElement groupElement) {
        if (ringElement.pRing.equals(pGroup.pRing)
            && groupElement.pGroup.equals(pGroup)) {

            return groupElement.exp(ringElement);
        } else {
            throw new ArithmError("Input not contained in domain!");
        }
    }
}
