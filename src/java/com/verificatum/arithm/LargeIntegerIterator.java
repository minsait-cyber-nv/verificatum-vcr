
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.arithm;

/**
 * Interface for an iterator over a {@link LargeIntegerArray}.
 *
 * @author Douglas Wikstrom
 */
public interface LargeIntegerIterator {

    /**
     * Returns the next integer, or null if no more integers are
     * available.
     *
     * @return Next integer.
     */
    LargeInteger next();

    /**
     * Returns true or false depending on if there is another integer
     * to read.
     *
     * @return True or false depending on if there is another integer
     *         to read.
     */
    boolean hasNext();

    /**
     * Deallocates any resources allocated by this instance. This
     * method is called automatically when {@link #hasNext()} returns
     * false, but it must be called explicitly if not all elements are
     * processed.
     */
    void close();
}
