
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.util;

import java.util.Map;
import java.util.TreeMap;

import com.verificatum.test.TestParameters;


/**
 * Tests the minimal JSON implementation.
 *
 * @author Douglas Wikstrom
 */
// PMD_ANNOTATION @SuppressWarnings("PMD.SignatureDeclareThrowsException")
public final class TestSimpleJSON {

    /**
     * Constructor needed to avoid that this class is instantiated.
     */
    private TestSimpleJSON() {
    }

    /**
     * Verify conversion to and from JSON.
     *
     * @param tp Test parameters configuration of the servers.
     * @throws Exception when failing test.
     */
    public static void toAndFromJSON(final TestParameters tp) throws Exception {

        final TreeMap<String, String> map1 = new TreeMap<String, String>();

        map1.put("hej1", "hopp1");
        map1.put("hej2", "hopp2");
        map1.put("hej3", "hopp3");

        final String mapString1 = SimpleJSON.toJSON(map1);

        final Map<String, String> map2 = SimpleJSON.readMap(mapString1);

        final String mapString2 = SimpleJSON.toJSON(map2);

        assert mapString1.equals(mapString2)
            : "Failed to convert to and from JSON map!";
    }
}
