
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.util;

import java.util.ArrayList;
import java.util.List;

/**
 * Keeps {@link UniformExecutor} instances containing exactly the same
 * number of threads as there are cores on the machine. This is a
 * package level class and is not intended to be used
 * directly. Instead {@link SplitWorker} and {@link ArrayWorker} uses
 * this class to get a thread pool to use during a single
 * multi-threaded computation.
 *
 * <p>
 *
 * The idea is to hide the fact that threads are re-used from the rest
 * of the applications, i.e., instances of the mentioned classes could
 * have been implemented such that threads were created on the fly.
 *
 * @author Douglas Wikstrom
 */
public final class UniformExecutors {

    /**
     * Prevents accidental instantiation.
     */
    private UniformExecutors() {
    }

    /**
     * Stores created executors. For a typical single story-line
     * applications, there will be at most one executor in this list
     * at any time.
     */
    private static List<UniformExecutor> executors =
        new ArrayList<UniformExecutor>();

    static {
        executors.add(get());
    }

    /**
     * Returns an executor with threads with maximum priority.
     *
     * @return Executor with threads of maximum priority.
     */
    static UniformExecutor get() {
        synchronized (executors) {

            if (executors.isEmpty()) {

                final int cores = Runtime.getRuntime().availableProcessors();
                return new UniformExecutor(cores);

            } else {

                return executors.remove(0);
            }
        }
    }

    /**
     * Returns an executor after use.
     *
     * @param executor A uniform executor ready to be re-used.
     */
    static void put(final UniformExecutor executor) {
        synchronized (executors) {
            executors.add(executor);
        }
    }
}
