
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.util;

import com.verificatum.crypto.Hashdigest;
import com.verificatum.crypto.HashfunctionHeuristic;
import com.verificatum.eio.ByteTreeConvertible;
import com.verificatum.eio.ExtIO;

/**
 * Various utility functions.
 *
 * @author Douglas Wikstrom
 */
public final class Functions {

    /**
     * Avoid accidental instantiation.
     */
    private Functions() {
    }

    /**
     * Default implementation of hash code for objects that can be
     * converted to a byte tree. This is slow, but very conservative.
     *
     * @param convertible Convertible object.
     * @return Hash code.
     */
    public static int hashCode(final ByteTreeConvertible convertible) {

        // This is a correct, but very slow implementation.

        final HashfunctionHeuristic hh = new HashfunctionHeuristic("SHA-256");
        final Hashdigest hd = hh.getDigest();
        convertible.toByteTree().update(hd);
        final byte[] d = hd.digest();
        return ExtIO.readInt(d, 0);
    }
}
