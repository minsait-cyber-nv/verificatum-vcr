
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.util;

/**
 * Simplistic timer that allows setting a point in time in the future
 * and then later querying if this future time has become now.
 *
 * @author Douglas Wikstrom
 */
public final class Timer {

    /**
     * Time from which this timer reports that the time is up.
     */
    private long endTime;

    /**
     * Creates an instance.
     *
     * @param timeToEnd Total time to wait.
     */
    public Timer(final long timeToEnd) {
        if (timeToEnd >= 0) {
            endTime = System.currentTimeMillis() + timeToEnd;
        } else {
            endTime = -1;
        }
    }

    /**
     * Returns true when the time is up.
     *
     * @return <code>true</code> if the time is up and
     * <code>false</code> otherwise.
     */
    public boolean timeIsUp() {
        if (endTime < 0) {
            return false;
        } else {
            return remainingTime() == 0;
        }
    }

    /**
     * Returns the remaining number of milliseconds until the time is
     * up, or zero if the time is already up.
     *
     * @return Remaining time until the time is up.
     */
    public long remainingTime() {
        if (endTime < 0) {
            return -1;
        } else {

            final long r = endTime - System.currentTimeMillis();

            if (r <= 0) {
                return 0;
            } else {
                return r;
            }
        }
    }
}
