
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.util;

/**
 * Triple of objects.
 *
 * @param <F> Type of first member.
 * @param <S> Type of second member.
 * @param <T> Type of third member.
 *
 * @author Douglas Wikstrom
 */
public class Triple<F, S, T> {

    /**
     * First member.
     */
    public F first;

    /**
     * Second member.
     */
    public S second;

    /**
     * Third member.
     */
    public T third;

    /**
     * Creates a triple.
     *
     * @param first First member.
     * @param second Second member.
     * @param third Third member.
     */
    public Triple(final F first, final S second, final T third) {
        this.first = first;
        this.second = second;
        this.third = third;
    }
}
