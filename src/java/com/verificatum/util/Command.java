
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.util;

import java.io.InputStream;
import java.io.IOException;

import com.verificatum.eio.ExtIO;

/**
 * Wrapper of command execution functionality in JVM.
 *
 * @author Douglas Wikstrom
 */
public final class Command {

    /**
     * Avoid accidental instantiation.
     */
    private Command() { }

    /**
     * Execute a command and return a pair of the exit code and
     * output as a string.
     *
     * @param command Command to be executed.
     * @return Pair consisting of the exit code and output of the
     * command.
     * @throws UtilException If the execution of the command fails.
     */
    public static Pair<Integer, String> execute(final String ... command)
        throws UtilException {
        try {
            Process process = null;
            InputStream is = null;
            try {
                final ProcessBuilder pb = new ProcessBuilder(command);
                pb.redirectErrorStream(true);
                process = pb.start();

                // final Runtime runTime = Runtime.getRuntime();
                // process = runTime.exec(command);
                is = process.getInputStream();

                final String output = ExtIO.readString(is);
                try {
                    process.waitFor();
                } catch (final InterruptedException ie) {
                    throw new UtilException("Native call was interrupted!", ie);
                }
                return new Pair<Integer, String>(process.exitValue(), output);
            } finally {
                ExtIO.strictClose(is);
            }
        } catch (final IOException ioe) {
            throw new UtilException("Failed to execute command!", ioe);
        }
    }

    /**
     * Execute a command and return the output.
     *
     * @param command Command to be executed.
     * @return Pair consisting of the exit code and output of the
     * command.
     * @throws UtilException If the execution of the command fails, or
     * if the exit code is non-zero.
     */
    public static String checkedExecute(final String ... command)
        throws UtilException {
        final Pair<Integer, String> output = execute(command);
        if (output.first == 0) {
            return output.second;
        } else {
            throw new UtilException("Non-zero exit code! ("
                                    + output.first + ")");
        }
    }
}
