
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.crypto;

import com.verificatum.eio.Marshalizable;

/**
 * Interface representing a secret key of a cryptosystem with labels.
 *
 * @author Douglas Wikstrom
 */
public interface CryptoSKey extends Marshalizable {

    /**
     * Decrypts the given ciphertext.
     *
     * @param label Label associated with the ciphertext.
     * @param ciphertext Ciphertext to be decrypted.
     * @return Resulting plaintext.
     */
    byte[] decrypt(byte[] label, byte[] ciphertext);
}
