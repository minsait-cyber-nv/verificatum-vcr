
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.crypto;

/**
 * Interface for a digest of a collision-free hash function.
 *
 * @author Douglas Wikstrom
 */
public interface Hashdigest {

    /**
     * Update the digest with more data.
     *
     * @param data Data to be hashed.
     */
    void update(byte[]... data);

    /**
     * Update the digest with more data.
     *
     * @param data Data to be hashed.
     * @param offset Offset of first byte to be processed.
     * @param length Number of bytes to process.
     */
    void update(byte[] data, int offset, int length);

    /**
     * Finalizes and returns the digest.
     *
     * @return Digest.
     */
    byte[] digest();
}
