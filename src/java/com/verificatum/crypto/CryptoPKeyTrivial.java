
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.crypto;

import java.util.Arrays;

import com.verificatum.eio.ByteTree;
import com.verificatum.eio.ByteTreeBasic;
import com.verificatum.eio.ByteTreeReader;
import com.verificatum.ui.Util;


/**
 * A trivial encryption key that maps a message to itself. This is
 * useful as a fallback to simplify the logics of protocols.
 *
 * @author Douglas Wikstrom
 */
public final class CryptoPKeyTrivial implements CryptoPKey {

    /**
     * Constructs an instance corresponding to the input
     * representation.
     *
     * @param btr Representation of an instance.
     * @param rs Random source used to probabilistically check the
     * validity of an input.
     * @param certainty Certainty with which an input is deemed
     * correct, i.e., an incorrect input is accepted with
     * probability at most 2<sup>- <code>certainty</code>
     * </sup>.
     * @return Public key represented by the input.
     */
    public static CryptoPKeyTrivial newInstance(final ByteTreeReader btr,
                                                final RandomSource rs,
                                                final int certainty) {
        return new CryptoPKeyTrivial();
    }

    // Documented in CryptoPKey.java

    @Override
    public byte[] encrypt(final byte[] label,
                          final byte[] message,
                          final RandomSource randomSource,
                          final int statDist) {
        return Arrays.copyOfRange(message, 0, message.length);
    }

    // Documented in ByteTreeConvertible.java

    @Override
    public ByteTreeBasic toByteTree() {
        return new ByteTree();
    }

    // Documented in Marshalizable.java

    @Override
    public String humanDescription(final boolean verbose) {
        return Util.className(this, verbose);
    }
}
