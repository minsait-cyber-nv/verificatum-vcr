
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.crypto;

import com.verificatum.eio.EIOException;
import com.verificatum.eio.Marshalizer;
import com.verificatum.ui.gen.GenException;
import com.verificatum.ui.gen.Generator;
import com.verificatum.ui.gen.GeneratorTool;
import com.verificatum.ui.opt.Opt;

/**
 * Extracts a public key from a key pair.
 *
 * @author Douglas Wikstrom
 */
public class SignaturePKeyGen implements Generator {

    /**
     * Default statistical distance.
     */
    static final int DEFAULT_CERT = 100;

    /**
     * Generates an option instance containing suitable options and
     * description.
     *
     * @return Option instance representing valid inputs to this
     *         instance.
     */
    protected Opt opt() {
        final Opt opt =
            GeneratorTool.defaultOpt(SignaturePKey.class.getSimpleName(), 1);

        opt.setUsageComment("(where " + SignaturePKey.class.getSimpleName()
                            + " = " + SignaturePKey.class.getName() + ")");

        opt.addParameter("keypair", "Signature key pair "
                         + "(com.verificatum.crypto.SignatureKeyPair).");
        opt.addOption("-cert", "value",
                      "Determines the probability that the input key pair "
                      + "is malformed, i.e., a value "
                      + "of t gives a bound of 2^(-t)." + " Defaults to "
                      + DEFAULT_CERT + ".");

        final String s = "Extracts a signature public key from a key pair.";

        opt.appendToUsageForm(1, "#-cert#keypair#");
        opt.appendDescription(s);

        return opt;
    }

    // Documented in com.verificatum.crypto.Generator.java.

    @Override
    public String gen(final RandomSource randomSource, final String[] args)
        throws GenException {

        final Opt opt = opt();

        final String res = GeneratorTool.defaultProcess(opt, args);
        if (res != null) {
            return res;
        }

        final String keypairString = opt.getStringValue("keypair");
        final int certainty = opt.getIntValue("-cert", DEFAULT_CERT);

        try {
            final SignatureKeyPair keypair =
                Marshalizer.unmarshalHexAux_SignatureKeyPair(keypairString,
                                                             randomSource,
                                                             certainty);

            return Marshalizer.marshalToHexHuman(keypair.getPKey(),
                                                 opt.getBooleanValue("-v"));
        } catch (final EIOException eioe) {
            throw new GenException("Malformed key pair!", eioe);
        }
    }

    @Override
    public String briefDescription() {
        return "Signature public key extractor.";
    }
}
