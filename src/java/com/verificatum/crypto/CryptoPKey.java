
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.crypto;

import com.verificatum.eio.Marshalizable;

/**
 * Interface representing a public key of a cryptosystem with labels.
 *
 * <p>
 *
 * <b>If you implement this interface you MUST override
 * {@link Object#equals(Object)} as well. Unfortunately, it is not
 * possible to enforce this using interfaces in Java.</b>
 *
 * @author Douglas Wikstrom
 */
public interface CryptoPKey extends Marshalizable {

    /**
     * Encrypts the given message using randomness from the given
     * source.
     *
     * @param label Label used when encrypting.
     * @param message Message to be encrypted.
     * @param randomSource Source of randomness.
     * @param statDist Allowed statistical error from ideal
     * distribution.
     * @return Resulting ciphertext.
     */
    byte[] encrypt(byte[] label, byte[] message,
                   RandomSource randomSource, int statDist);
}
