
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.crypto;

import com.verificatum.eio.ExtIO;

/**
 * Digest for a random oracle.
 *
 * @author Douglas Wikstrom
 */
public final class HashdigestRandomOracle implements Hashdigest {

    /**
     * Underlying hashfunction.
     */
    Hashfunction hashfunction;

    /**
     * Message digest wrapped by this instance.
     */
    Hashdigest hd;

    /**
     * Output bit length.
     */
    int outputLength;

    /**
     * Creates an instance using the given hashfunction and with the
     * given output bit length.
     *
     * @param hashfunction Underlying hashfunction.
     * @param outputLength Output bit length.
     */
    public HashdigestRandomOracle(final Hashfunction hashfunction,
                                  final int outputLength) {

        this.hashfunction = hashfunction;
        this.outputLength = outputLength;

        final byte[] prefix = new byte[4];
        ExtIO.writeInt(prefix, 0, outputLength);

        this.hd = hashfunction.getDigest();
        hd.update(prefix);
    }

    // Documented in Hashdigest.java

    @Override
    public void update(final byte[]... data) {
        hd.update(data);
    }

    @Override
    public void update(final byte[] data,
                       final int offset,
                       final int length) {
        hd.update(data, offset, length);
    }

    @Override
    public byte[] digest() {

        final PRGHeuristic prg = new PRGHeuristic(hashfunction);

        final byte[] seed = hd.digest();

        prg.setSeed(seed);

        final int len = (outputLength + 7) / 8;

        final byte[] res = prg.getBytes(len);

        if (outputLength % 8 != 0) {
            res[0] = (byte) (res[0] & (0xFF >>> (8 - outputLength % 8)));
        }

        return res;
    }
}
