
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.crypto;

/**
 * Abstract base class for a collision-free hash function with fixed
 * input and output sizes. If the input length is not a multiple of 8,
 * then the appropriate number of bits in the first input byte are
 * ignored.
 *
 * @author Douglas Wikstrom
 */
public interface HashfunctionFixedLength extends HashfunctionBasic {

    /**
     * Evaluates the function on the given input. If the output length
     * is not a multiple of 8, the suitable number of bits in the
     * first output byte are set to zero.
     *
     * @param data Input data.
     * @return Output of hash function.
     */
    byte[] hash(byte[] data);

    /**
     * Returns the input bit length.
     *
     * @return Input length in bits.
     */
    int getInputLength();
}
