
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.crypto;

import com.verificatum.eio.Marshalizer;
import com.verificatum.ui.gen.GenException;
import com.verificatum.ui.gen.Generator;
import com.verificatum.ui.gen.GeneratorTool;
import com.verificatum.ui.opt.Opt;

/**
 * Generator of the key generation algorithm of the standard RSA
 * signature scheme based on SHA-256.
 *
 * @author Douglas Wikstrom
 */
public final class SignatureKeyGenHeuristicGen implements Generator {

    /**
     * Generates an option instance containing suitable options and
     * description.
     *
     * @return Option instance representing valid inputs to this
     *         instance.
     */
    protected Opt opt() {
        final Opt opt = GeneratorTool.
            defaultOpt(SignatureKeyGenHeuristic.class.getSimpleName(), 1);

        opt.setUsageComment("(where "
                            + SignatureKeyGenHeuristic.class.getSimpleName()
                            + " = "
                            + SignatureKeyGenHeuristic.class.getName() + ")");

        opt.addParameter("bitlength", "Bits in modulus.");

        final String s =
            "Generates an instance of a full domain hash (SHA-256) "
            + "RSA signature key generator for keys of the given key length";

        opt.appendToUsageForm(1, "##bitlength#");
        opt.appendDescription(s);

        return opt;
    }

    // Documented in com.verificatum.crypto.Generator.java.

    @Override
    public String gen(final RandomSource randomSource, final String[] args)
        throws GenException {

        final Opt opt = opt();

        final String res = GeneratorTool.defaultProcess(opt, args);
        if (res != null) {
            return res;
        }

        final int bitlength = opt.getIntValue("bitlength");

        final SignatureKeyGenHeuristic keygen =
            new SignatureKeyGenHeuristic(bitlength);

        return Marshalizer.marshalToHexHuman(keygen, opt.getBooleanValue("-v"));
    }

    @Override
    public String briefDescription() {
        return "RSA signature key generator.";
    }
}
