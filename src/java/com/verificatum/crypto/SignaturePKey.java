
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.crypto;

import com.verificatum.eio.Marshalizable;

/**
 * Interface representing a public signature key.
 *
 * @author Douglas Wikstrom
 */
public interface SignaturePKey extends Marshalizable {

    /**
     * Verify the given signature and message using this public key.
     *
     * @param signature Candidate signature.
     * @param message Data that supposedly is signed.
     * @return Verdict for the signature and message pair.
     */
    boolean verify(byte[] signature, byte[]... message);

    /**
     * Verify the given signature and digest using this public key.
     * The result is undefined unless {@link #getDigest()} was used to
     * compute the digest.
     *
     * @param signature Candidate signature.
     * @param d Digest that supposedly is signed.
     * @return Verdict for the signature and message pair.
     */
    boolean verifyDigest(byte[] signature, byte[] d);

    /**
     * Returns an updateable digest that can later be given as input
     * to {@link #verify(byte[],byte[][])}.
     *
     * @return Updateable digest.
     */
    Hashdigest getDigest();
}
