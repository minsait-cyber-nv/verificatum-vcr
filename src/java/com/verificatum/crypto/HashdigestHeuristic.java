
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.crypto;

import java.security.MessageDigest;

/**
 * Interface for a digest of a collision-free hash function.
 *
 * @author Douglas Wikstrom
 */
public final class HashdigestHeuristic implements Hashdigest {

    /**
     * Message digest wrapped by this instance.
     */
    MessageDigest md;

    /**
     * Constructs a wrapper for the given message digest.
     *
     * @param md Message digest wrapped by this instance.
     */
    public HashdigestHeuristic(final MessageDigest md) {
        this.md = md;
    }

    // Documented in Hashdigest.java

    @Override
    public void update(final byte[]... data) {
        for (int i = 0; i < data.length; i++) {
            md.update(data[i]);
        }
    }

    @Override
    public void update(final byte[] data, final int offset, final int length) {
        md.update(data, offset, length);
    }

    @Override
    public byte[] digest() {
        return md.digest();
    }
}
