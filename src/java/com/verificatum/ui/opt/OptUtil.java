
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.ui.opt;

import com.verificatum.vcr.VCR;

/**
 * Utility functions for command line options.
 *
 * @author Douglas Wikstrom
 */
public final class OptUtil {

    /**
     * Method to prevent instantiation of this class.
     */
    private OptUtil() {
    }

    /**
     * Process help and version flags in a standard way and return the
     * description.
     *
     * @param opt Parsed command line parameters.
     * @return String describing usage information or version, or null
     * if no flag was given in the command line parameters.
     */
    public static String processHelpAndVersionString(final Opt opt) {

        // Output usage info.
        if (opt.getBooleanValue("-h")) {
            return opt.usage();
        }

        // Output version.
        if (opt.getBooleanValue("-version")) {
            return VCR.version();
        }
        return null;
    }

    /**
     * Process help and version flags in a standard way and print the
     * description.
     *
     * @param opt Parsed command line parameters.
     */
    public static void processHelpAndVersion(final Opt opt) {

        final String res = processHelpAndVersionString(opt);

        if (res != null) {
            System.out.println(res);
            System.exit(0);
        }
    }
}
