
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.ui;

import java.io.InputStream;
import java.io.PrintStream;

/**
 * Uniform container of basic textual input and output streams that
 * are found on consoles.
 *
 * @author Douglas Wikstrom
 */
public class Console {

    /**
     * Standard input stream.
     */
    public InputStream in;

    /**
     * Standard output stream.
     */
    public PrintStream out;

    /**
     * Standard error stream.
     */
    public PrintStream err;

    /**
     * This prevents initialization.
     *
     * @param in Standard input.
     * @param out Standard output.
     * @param err Standard error.
     */
    protected Console(final InputStream in, final PrintStream out,
                      final PrintStream err) {
        this.in = in;
        this.out = out;
        this.err = err;
    }
}
