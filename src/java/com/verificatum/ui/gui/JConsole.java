
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.ui.gui;

import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.PrintStream;

/**
 * Implements a simple console which connects standard input, output
 * and error to a {@link javax.swing.JTextArea}. One would
 * expect that such functionality existed in the standard Java API,
 * but it seems this is not the case.
 *
 * <b>WARNING! Read the documentation of {@link JTextualUI} before
 * use.</b>
 *
 * @author Douglas Wikstrom
 */
public class JConsole extends com.verificatum.ui.Console {

    /**
     * Connects a <code>JTextArea</code> to an input pipe stream.
     */
    private final JConsoleInLinker inLinker; // NOPMD Used in subclasses.

    /**
     * Connects a <code>JTextArea</code> to an output pipe stream.
     */
    private final JConsoleOutLinker outLinker; // NOPMD Used in subclasses.

    /**
     * Connects a <code>JTextArea</code> to an error pipe stream.
     */
    private final JConsoleOutLinker errLinker; // NOPMD Used in subclasses.

    /**
     * Creates a console and connects it to the given
     * <code>JTextArea</code>.
     *
     * @param jTextArea <code>JTextArea</code> to which this instance
     * is connected.
     */
    public JConsole(final FixedJTextArea jTextArea) {
        super(null, null, null);

        // Make sure user can not enter anything directly on the
        // JTextArea.
        jTextArea.setEditable(false);

        // Create pipes.
        final PipedInputStream pin = new PipedInputStream();
        final PipedOutputStream pout = new PipedOutputStream();
        final PipedOutputStream perr = new PipedOutputStream();

        // Link them to the JTextArea.
        inLinker = new JConsoleInLinker(jTextArea, pin);
        outLinker = new JConsoleOutLinker(pout, jTextArea);
        errLinker = new JConsoleOutLinker(perr, jTextArea);

        // Connect the pipes to workable streams.
        in = pin;
        out = new PrintStream(pout);
        err = new PrintStream(perr);

    }
}
