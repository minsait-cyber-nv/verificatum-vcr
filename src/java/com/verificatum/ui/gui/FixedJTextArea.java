
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.ui.gui;

import javax.swing.JTextArea;

/**
 * Adds the ability to add a selection listener to {@link JTextArea}.
 *
 * @author Douglas Wikstrom
 */
public final class FixedJTextArea extends JTextArea {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * Listener of this instance.
     */
    private SelectionListener sl;

    /**
     * Constructs an instance of the given size.
     *
     * @param defaultRows Default number of rows.
     * @param defaultColumns Default number of columns.
     */
    public FixedJTextArea(final int defaultRows, final int defaultColumns) {
        super(defaultRows, defaultColumns);
        this.sl = null;
    }

    /**
     * Adds a selection listener to this instance.
     *
     * @param sl Listener to be associated with this instance.
     */
    public void addSelectionListener(final SelectionListener sl) {
        this.sl = sl;
    }

    // Documented in SelectionListener.java

    @Override
    public void replaceSelection(final String content) {
        if (sl != null) {
            sl.replaceSelection(content);
        }
        super.replaceSelection(content);
    }
}
