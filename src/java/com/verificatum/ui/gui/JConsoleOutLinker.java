
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.ui.gui;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

import javax.swing.JTextArea;

import com.verificatum.ui.UIError;


/**
 * Links a <code>PipedOutputStream</code> to write to a
 * <code>JTextArea</code>, i.e., it allows us to write to a text area
 * by means of a stream.
 *
 * <b>WARNING! Read the documentation of {@link JTextualUI} before
 * use.</b>
 *
 * @author Douglas Wikstrom
 */
public final class JConsoleOutLinker implements Runnable {

    /**
     * Internal buffer.
     */
    static final int BUFFER_SIZE = 1024;

    /**
     * A pipe to allow writing to the text area.
     */
    private PipedInputStream pin;

    /**
     * Text area to which we write.
     */
    private final JTextArea jTextArea;

    /**
     * A thread that keeps writing from the stream onto the text area.
     */
    private final Thread thisThread;

    /**
     * Creates an instance.
     *
     * @param pout Source of inputs
     * @param jTextArea Where the inputs are written.
     */
    public JConsoleOutLinker(final PipedOutputStream pout,
                             final JTextArea jTextArea) {
        try {
            this.pin = new PipedInputStream(pout);
        } catch (final IOException ioe) {
            throw new UIError("Unable to create input stream!", ioe);
        }
        this.jTextArea = jTextArea;

        thisThread = new Thread(this);
        thisThread.setDaemon(true);
        thisThread.start();
    }

    // Documented in super class or interface.

    @Override
    public void run() {

        final byte[] buffer = new byte[BUFFER_SIZE];

        for (;;) {
            try {
                synchronized (this) {
                    this.wait(100);
                }
            } catch (final InterruptedException ie) {
            }

            int len = 0;
            try {
                final int noBytes = pin.available();

                if (noBytes > 0) {
                    len =
                        pin.read(buffer, 0, Math.min(noBytes, BUFFER_SIZE));
                    if (len > 0) {
                        jTextArea.append(new String(buffer, 0, len));
                        jTextArea
                            .setCaretPosition(jTextArea.getText().length());
                    }
                }
            } catch (final IOException ioe) {
                throw new UIError("Unable to read from input stream! "
                                  + ioe.getMessage(),
                                  ioe);
            }
        }
    }
}
