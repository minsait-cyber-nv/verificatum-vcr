
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.ui.info;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Network data field that can be used in an XML configuration file.
 *
 * @author Douglas Wikstrom
 */
public abstract class NetField extends InfoField {

    /**
     * Type of net field.
     */
    private final String type;

    /**
     * Regular expression for validation.
     */
    private final String pattern;

    /**
     * Creates an instance.
     *
     * @param name Name of instance.
     * @param description Description of this field.
     * @param minOccurs Minimum number of times the field must occur
     * in the context it is used (inklusive lower bound).
     * @param maxOccurs Strict upper bound on the number of times the
     * field may occur in the context it is used (exlusive
     * upper bound).
     * @param type Type of net field.
     * @param pattern Pattern for a string representing a location on
     * the Internet.
     */
    public NetField(final String name,
                    final String description,
                    final int minOccurs,
                    final int maxOccurs,
                    final String type,
                    final String pattern) {
        super(name, description, minOccurs, maxOccurs);
        this.type = type;
        this.pattern = pattern;
    }

    // These methods are documented in the super class.

    @Override
    public String schemaElementString() {
        return "<xs:element name=\"" + name + "\" "
            + "type=\"" + type + "\"/>";
    }

    /**
     * Validate that the value satisfies the restrictions.
     *
     * @param value String to be validated.
     *
     * @throws InfoException If the input string does not satisfy the
     * restrictions.
     */
    public void validate(final String value) throws InfoException {
        final Pattern p = Pattern.compile(pattern);
        final Matcher m = p.matcher(value);
        if (!m.matches()) {
            throw new InfoException("Value does not match expression! "
                                    + "(" + value + " is not " + type + ")");
        }
    }

    @Override
    public Object parse(final String value) throws InfoException {
        validate(value);
        return value;
    }
}
