
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.ui.info;

import java.util.ArrayList;
import java.util.List;

/**
 * Factory for creating <code>PartyInfo</code> instances.
 *
 * @author Douglas Wikstrom
 */
public final class PartyInfoFactory {

    /**
     * Additional fields.
     */
    List<InfoField> additionalFields;

    /**
     * Creates a factory that creates <code>PartyInfo</code> instances
     * with the given additional fields.
     *
     * @param infoFields Additional info fields.
     */
    public PartyInfoFactory(final InfoField... infoFields) {
        additionalFields = new ArrayList<InfoField>();
        addInfoFields(infoFields);
    }

    /**
     * Adds the given info fields to this instance.
     *
     * @param infoFields Additional info fields.
     */
    public void addInfoFields(final InfoField... infoFields) {
        for (int i = 0; i < infoFields.length; i++) {
            additionalFields.add(infoFields[i]);
        }
    }

    /**
     * Creates an instance of <code>PartyInfo</code>.
     *
     * @return A new instance.
     */
    public PartyInfo newInstance() {

        final int size = additionalFields.size();

        return new PartyInfo(additionalFields.toArray(new InfoField[size]));
    }
}
