
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.ui.info;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;

import com.verificatum.eio.ExtIO;


/**
 * Abstract base class for a root <code>Info</code> instance.
 *
 * @author Douglas Wikstrom
 */
public abstract class RootInfo extends Info {

    /**
     * Name of version tag.
     */
    public static final String VERSION = "version";

    /**
     * Description of version field.
     */
    public static final String VERSION_DESCRIPTION =
        "Version of Verificatum Software for which this info is intended.";

    /**
     * Creates an empty instance with no <code>InfoField</code>s and
     * no values.
     */
    protected RootInfo() {
        super();
        final StringField versionField =
            new StringField(VERSION, VERSION_DESCRIPTION, 1, 1).
            setPattern("[0-9]{0,3}\\.[0-9]{0,3}\\.[0-9]{0,3}");
        addInfoField(versionField);
    }

    /**
     * Parses the file with the given filename according to its own
     * schema and stores the values it encounters.
     *
     * @param infoFile XML file.
     * @return Instance containing values parsed from file.
     * @throws InfoException If parsing fails.
     */
    public RootInfo parse(final File infoFile) throws InfoException {
        (new InfoParser()).parse(this, infoFile);
        return this;
    }

    /**
     * Parses the file with the given filename according to its own
     * schema and stores the values it encounters.
     *
     * @param infoFilename Name of XML file.
     * @return Instance containing values parsed from file.
     * @throws InfoException If parsing fails.
     */
    public RootInfo parse(final String infoFilename) throws InfoException {
        return parse(new File(infoFilename));
    }

    /**
     * Generates its own schema.
     *
     * @return Schema of this instance.
     */
    public abstract String generateSchema();

    /**
     * Outputs an XML representation of the values in this instance.
     *
     * @return XML code representing this instance.
     */
    protected abstract String toXML();

    /**
     * Writes an XML representation of the values in this instance to
     * the given file.
     *
     * @param infoFile Where the XML is written.
     * @throws InfoException If generating the info file fails.
     */
    public void toXML(final File infoFile) throws InfoException {

        BufferedWriter bw = null;
        try {

            if (!ExtIO.delete(infoFile)) {
                throw new InfoError("Unable to delete existing file!");
            }

            bw = ExtIO.getBufferedWriter(infoFile);
            bw.write(toXML());
            bw.flush();

        } catch (final IOException ioe) {
            throw new InfoException("Can not generate info file!", ioe);
        } finally {
            ExtIO.strictClose(bw);
        }
        if (!infoFile.setWritable(false, false)) {
            throw new InfoException("Unable to make info file read only!");
        }
    }
}
