
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.ui.info;

/**
 * Defines a few XS types to be used in info schemas in terms of
 * regular expressions that can also be used programatically.
 *
 * @author Douglas Wikstrom
 */
public final class XSTypes {

    /**
     * Prevents accidental instantiation.
     */
    private XSTypes() {
    }

    /**
     * String pattern for validating port number in [1,65535].
     */
    public static final String PORT_PATTERN =
        "("
        +   "(6553[0-5])"
        +   "|(655[0-2][0-9])"
        +   "|(65[0-4][0-9]{2})"
        +   "|(6[0-4][0-9]{3})"
        +   "|([1-5][0-9]{4})"
        +   "|([1-9][0-9]{0,3})"
        + ")";

    /**
     * String pattern for validating IP number.
     */
    public static final String IPV4_PATTERN =
        "("
        +   "("
        +     "(25[0-5])"
        +     "|(2[0-4][0-9])"
        +     "|(1[0-9][0-9])"
        +     "|([1-9][0-9])"
        +     "|[0-9]"
        +   ")"
        + "\\.){3}"
        + "("
        +   "(25[0-5])"
        +   "|(2[0-4][0-9])"
        +   "|(1[0-9][0-9])"
        +   "|([1-9][0-9])"
        +   "|[0-9]"
        + ")";

    /**
     * String pattern for isolated hostname.
     */
    public static final String HOST_PATTERN =
        "([a-z0-9]([-a-z0-9]*[a-z0-9])*)"
        + "(\\.([a-z0-9]([-a-z0-9]*[a-z0-9])*))*";

    /**
     * Inet with port pattern.
     */
    public static final String INET_PORT_PATTERN =
        "((" + HOST_PATTERN + ")|(" + IPV4_PATTERN + "))" + ":" + PORT_PATTERN;

    /**
     * URL with port pattern.
     */
    public static final String URL_PORT_PATTERN = "http://" + INET_PORT_PATTERN;

    /**
     * XSD types for IP addresses, port numbers, and hostnames.
     */
    public static final String IP =
        "<xs:simpleType name=\"inetport\">\n"
        + "  <xs:restriction base=\"xs:string\">\n"
        + "    <xs:maxLength value=\"512\"/>\n"
        + "    <xs:pattern value=\"" + INET_PORT_PATTERN + "\"/>\n"
        + "  </xs:restriction>\n"
        + "</xs:simpleType>\n"
        + "\n"
        + "<xs:simpleType name=\"urlport\">\n"
        + "  <xs:restriction base=\"xs:string\">\n"
        + "    <xs:maxLength value=\"512\"/>\n"
        + "    <xs:pattern value=\"" + URL_PORT_PATTERN + "\"/>\n"
        + "  </xs:restriction>\n"
        + "</xs:simpleType>";
}
