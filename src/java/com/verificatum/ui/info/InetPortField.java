
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.ui.info;

import java.net.InetSocketAddress;

/**
 * Inet with port data field that can be used in an XML configuration
 * file.
 *
 * @author Douglas Wikstrom
 */
public class InetPortField extends NetField {

    /**
     * Creates an instance.
     *
     * @param name Name of instance.
     * @param description Description of this field.
     * @param minOccurs Minimum number of times the field must occur
     * in the context it is used (inklusive lower bound).
     * @param maxOccurs Strict upper bound on the number of times the
     * field may occur in the context it is used (exlusive
     * upper bound).
     */
    public InetPortField(final String name,
                         final String description,
                         final int minOccurs,
                         final int maxOccurs) {
        super(name, description, minOccurs, maxOccurs, "inetport",
              XSTypes.INET_PORT_PATTERN);
    }

    /**
     * Validate that input represents an inet address.
     *
     * @param value String to verify.
     *
     * @throws InfoException If the input does not represent an inet
     * address.
     */
    public void validate(final String value) throws InfoException {
        super.validate(value);
        final String[] s = value.split(":");
        if (s.length != 2) {
            throw new InfoException("Invalid InetPortField! (" + value + ")");
        }
        try {
            final int port = Integer.parseInt(s[1]);
            new InetSocketAddress(s[0], port);
        } catch (final NumberFormatException nfe) {
            throw new InfoException("Invalid port number! (" + s[1] + ")", nfe);
        }
    }
}
