
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.ui;

/**
 * Simple abstract user interface for protocols.
 *
 * @author Douglas Wikstrom
 */
public interface UI {

    /**
     * Returns the description string used to refer to the parties of
     * the executed protocol, e.g., "Party", "P", or "M".
     *
     * @return Description string.
     */
    String getDescrString();

    /**
     * Returns the description string used to refer to the
     * <code>i</code>th party in the protocol. This simply
     * concatenates the input integer onto the description string
     * output by {@link #getDescrString()}.
     *
     * @param i Index of party.
     * @return Description string.
     */
    String getDescrString(int i);

    /**
     * Presents the question string <code>msgString</code> to the user
     * who can reply by a <code>String</code> that is then returned.
     *
     * @param msgString Question string presented to the user.
     * @return Answer from user.
     */
    String stringQuery(String msgString);

    /**
     * Presents the question string <code>msgString</code> to the user
     * who can reply by <code>yes</code> or <code>no</code> that is
     * then returned in the form of a <code>boolean</code>.
     *
     * @param msgString Question string presented to the user.
     * @return <code>boolean</code> representing the answer.
     */
    boolean dialogQuery(String msgString);

    /**
     * Presents the question string <code>msgString</code> to the user
     * who can reply with an integer that is then returned.
     *
     * @param msgString Question string showed to the user.
     * @return Answer of user.
     */
    int intQuery(String msgString);

    /**
     * Presents several alternatives to the user who can reply by
     * choosing one of the alternatives. The index of the chosen
     * alternative is returned.
     *
     * @param alternatives Alternatives presented to the user.
     * @param descString General description.
     * @return Choice of user.
     */
    int alternativeQuery(String[] alternatives, String descString);

    /**
     * Returns the root logging context of the user interface.
     *
     * @return Logging context of this user interface.
     */
    Log getLog();

    /**
     * Present the given information to the user.
     *
     * @param str Message to display.
     */
    void print(String str);
}
