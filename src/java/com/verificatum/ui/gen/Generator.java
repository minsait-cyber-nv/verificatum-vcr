
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.ui.gen;

import com.verificatum.crypto.RandomSource;

/**
 * Interface representing the ability of a class to be used by
 * {@link GeneratorTool} to generate an instance based on some command
 * line arguments. This is used to generate basic cryptographic
 * objects, e.g., keys for signature schemes, groups in which the
 * discrete logarithm problem is hard, or provably secure
 * collision-free hashfunctions.
 *
 * @author Douglas Wikstrom
 */
public interface Generator {

    /**
     * Outputs a string representation of an instance generated
     * according to the input instructions.
     *
     * @param randomSource Source of randomness that can be used in
     * generation.
     * @param args Instructions for how to generate the instance.
     * @return Description of an instance.
     * @throws GenException If the parameters are malformed or if the
     *  given random source is <code>null</code> and a
     *  functioning random source is needed to generate the
     *  required instance. In this case the description of
     *  the exception should be suitable to present to the
     *  user as an error message.
     */
    String gen(RandomSource randomSource, String[] args)
        throws GenException;

    /**
     * Returns a brief description of the implementing class.
     *
     * @return Brief description of class.
     */
    String briefDescription();
}
