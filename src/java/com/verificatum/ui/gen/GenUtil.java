
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.ui.gen;

import com.verificatum.eio.TempFile;

/**
 * Utility functions.
 *
 * @author Douglas Wikstrom
 */
public final class GenUtil {

    /**
     * Method to prevent instantiation of this class.
     */
    private GenUtil() {
    }

    /**
     * Returns true or false depending on if the flag occurs on the
     * command line.
     *
     * @param flag Flag to search for.
     * @param args Command line parameters.
     * @return True or false depending on if the flag occurs on the
     * command line.
     */
    public static boolean specialFlag(final String flag, final String[] args) {
        for (int i = 1; i < args.length; i++) {
            if (args[i].equals(flag)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Process errors and print suitable error parameters.
     *
     * @param throwable Throwable to process.
     * @param cerrFlag If true, then a clean error message string is
     * printed without any formatting.
     * @param eFlag If true, then the exception causing the execution
     * is printed as a stack trace.
     */
    public static void processErrors(final Throwable throwable,
                                     final boolean cerrFlag,
                                     final boolean eFlag) {
        if (cerrFlag) {
            System.err.print(throwable.getMessage());
        } else {
            final String e = "\n" + "ERROR: " + throwable.getMessage() + "\n";
            System.err.println(e);
        }

        if (eFlag) {
            throwable.printStackTrace();
        }

        TempFile.free();
        System.exit(1);
    }
}
