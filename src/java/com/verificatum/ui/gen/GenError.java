
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Core Routines (VCR). VCR is NOT
 * free software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VCR.
 *
 * You should have agreed to this license and appendix when
 * downloading VCR and received a copy of the license and appendix
 * along with VCR. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VCR in any way and you must delete
 * VCR immediately.
 */

package com.verificatum.ui.gen;

/**
 * Thrown when a fatal error occurs. This should never be caught.
 *
 * @author Douglas Wikstrom
 */
public class GenError extends Error {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * Constructs a new error with the specified detail message.
     *
     * @param message Detailed message of the problem.
     */
    public GenError(final String message) {
        super(message);
    }

    /**
     * Constructs a new error with the specified detail message and
     * cause.
     *
     * @param message Detailed message of the problem.
     * @param cause What caused this error to be thrown.
     */
    public GenError(final String message, final Throwable cause) {
        super(message, cause);
    }
}
