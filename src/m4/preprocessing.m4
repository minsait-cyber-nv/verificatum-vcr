dnl Copyright 2008-2016 Douglas Wikstrom
dnl
dnl This file is part of Verificatum Core Routines (VCR). VCR is NOT
dnl free software. It is distributed under Verificatum License 1.0 and
dnl Verificatum License Appendix 1.0 for VCR.
dnl
dnl You should have agreed to this license and appendix when
dnl downloading VCR and received a copy of the license and appendix
dnl along with VCR. If not, then the license and appendix are available
dnl at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
dnl http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
dnl
dnl If you do not agree to the combination of this license and
dnl appendix, then you may not use VCR in any way and you must delete
dnl VCR immediately.
dnl
ifdef(`USE_VMGJ',`define(`VMGJ_CODE')',`define(`VMGJ_PURE_JAVA_CODE')')dnl
ifdef(`USE_VECJ',`define(`VECJ_CODE')',`define(`VECJ_PURE_JAVA_CODE')')dnl
dnl
ifdef(`VMGJ_PURE_JAVA_CODE',dnl
`define(`VMGJ_BEGIN',`Removed native code here.divert(-1)')dnl
define(`VMGJ_END',`divert')dnl
define(`VMGJ_PURE_JAVA_BEGIN',`Enabled pure java code begins here.')dnl
define(`VMGJ_PURE_JAVA_END',`Enabled pure java code ends here')')dnl
dnl
ifdef(`VMGJ_CODE',dnl
`define(`VMGJ_PURE_JAVA_BEGIN',`Removed pure java code here.divert(-1)')dnl
define(`VMGJ_PURE_JAVA_END',`divert')dnl
define(`VMGJ_BEGIN',`Enabled calls to native code begins here.')dnl
define(`VMGJ_END',`Enabled calls to native code ends here')')dnl
dnl
ifdef(`VECJ_PURE_JAVA_CODE',dnl
`define(`VECJ_BEGIN',`Removed native code here.divert(-1)')dnl
define(`VECJ_END',`divert')dnl
define(`VECJ_PURE_JAVA_BEGIN',`Enabled pure java code begins here.')dnl
define(`VECJ_PURE_JAVA_END',`Enabled pure java code ends here')')dnl
dnl
ifdef(`VECJ_CODE',dnl
`define(`VECJ_PURE_JAVA_BEGIN',`Removed pure java code here.divert(-1)')dnl
define(`VECJ_PURE_JAVA_END',`divert')dnl
define(`VECJ_BEGIN',`Enabled calls to native code begins here.')dnl
define(`VECJ_END',`Enabled calls to native code ends here')')dnl
dnl
undefine(`format')dnl
