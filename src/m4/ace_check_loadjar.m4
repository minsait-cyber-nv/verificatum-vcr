
# Copyright 2008-2016 Douglas Wikstrom
#
# This file is part of Verificatum Core Routines (VCR). VCR is NOT free
# software. It is distributed under Verificatum License 1.0 and
# Verificatum License Appendix 1.0 for VCR.
#
# You should have agreed to this license and appendix when
# downloading VCR and received a copy of the license and appendix
# along with VCR. If not, then the license and appendix are available
# at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
# http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
#
# If you do not agree to the combination of this license and
# appendix, then you may not use VCR in any way and you must delete
# VCR immediately.

# 1 Name of flag for configure.
# 2 Path to jar file.
# 3 Name of jar file for informational purposes.
# 4 Name of class to test for loading.
# 5 Expected version in jar file manifest.

AC_DEFUN([ACE_CHECK_LOADJAR],[
AC_REQUIRE([ACE_PROG_JAVA])

AC_ARG_ENABLE([check-$1],
     [  --disable-check-$1    Skip checking that $3 is installed.],
     [],[
ace_res=$($JAVA $JAVAFLAGS -classpath $2:tools/installation TestLoadJar $4 $5)

echo -n "checking for $3... "
if test "x$ace_res" = x;
then
   echo "yes"
else
   echo "no"
   AC_MSG_ERROR([$ace_res

Please make sure that $3 and its native libraries are installed.
])
fi
])
])
