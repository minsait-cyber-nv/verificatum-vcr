#!/bin/sh

# Copyright 2008-2016 Douglas Wikstrom
#
# This file is part of Verificatum Core Routines (VCR). VCR is NOT free
# software. It is distributed under Verificatum License 1.0 and
# Verificatum License Appendix 1.0 for VCR.
#
# You should have agreed to this license and appendix when
# downloading VCR and received a copy of the license and appendix
# along with VCR. If not, then the license and appendix are available
# at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
# http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VCR
#
# If you do not agree to the combination of this license and
# appendix, then you may not use VCR in any way and you must delete
# VCR immediately.


add_result() {

    TOOLNAME=$1
    CONTENTFILE=$2
    OUTPUTFILE=$3

    printf "\n#########################################################\n" \
>> $OUTPUTFILE
    printf "$TOOLNAME\n\n" >> $OUTPUTFILE

    CONTENT=`cat $CONTENTFILE`

    if test "x$CONTENT" = x;
    then
	printf "NO COMPLAINTS!\n" >> $OUTPUTFILE
    else
	printf "%s" "$CONTENT" >> $OUTPUTFILE
    fi
}

printf "\nCODE ANALYSIS REPORTS\n" > analysis_report.txt
add_result "Checkstyle (configured using checkstyle_ruleset.xml and checkstyle_suppressions.xml)" checkstyle/checkstyle_report.txt analysis_report.txt
add_result "Findbugs (configured using findbugs_configure.xml)" findbugs/findbugs_report.txt analysis_report.txt
add_result "PMD (configured using pmd_ruleset.xml and pmd_filter.sh)" pmd/pmd_report.txt analysis_report.txt

printf "\n"
